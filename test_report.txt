Name                                                              Stmts   Miss  Cover
-------------------------------------------------------------------------------------
__init__.py                                                           0      0   100%
backend\Accounts\__init__.py                                          0      0   100%
backend\Accounts\admin.py                                            21      2    90%
backend\Accounts\apps.py                                              8      0   100%
backend\Accounts\emails.py                                            8      0   100%
backend\Accounts\helpers.py                                          11      0   100%
backend\Accounts\migrations\0001_initial.py                           9      0   100%
backend\Accounts\migrations\__init__.py                               0      0   100%
backend\Accounts\models.py                                           29      0   100%
backend\Accounts\serializers.py                                     129      0   100%
backend\Accounts\signals.py                                           8      0   100%
backend\Accounts\urls.py                                              3      0   100%
backend\Accounts\views.py                                           113      0   100%
backend\Products\__init__.py                                          0      0   100%
backend\Products\admin.py                                             9      0   100%
backend\Products\apps.py                                              4      0   100%
backend\Products\migrations\0001_initial.py                           8      0   100%
backend\Products\migrations\__init__.py                               0      0   100%
backend\Products\models.py                                           14      0   100%
backend\Products\serializers.py                                      23      0   100%
backend\Products\urls.py                                              3      0   100%
backend\Products\views.py                                            43      0   100%
backend\__init__.py                                                   4      0   100%
backend\celery.py                                                     7      0   100%
backend\core\__init__.py                                              0      0   100%
backend\core\authentications\custom_JWT_token_authentication.py      39      4    90%
backend\core\custom_filter\logging_filter.py                          7      0   100%
backend\core\custom_lookups\negative_lookups.py                      27      0   100%
backend\core\email_utils\send_email.py                                9      0   100%
backend\core\generators\user_key_generator.py                         9      0   100%
backend\core\get_data\get_client_ip.py                                2      0   100%
backend\core\mixins\list_search_mixins.py                            84      0   100%
backend\core\permissions\permissions.py                              19      0   100%
backend\core\validators\model_validators.py                           4      0   100%
backend\core\validators\serializer_validators.py                      4      0   100%
backend\settings.py                                                  54      1    98%
backend\urls.py                                                      12      0   100%
manage.py                                                            24      4    83%
-------------------------------------------------------------------------------------
TOTAL                                                               748     11    99%
