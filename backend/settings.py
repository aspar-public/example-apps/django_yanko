import logging
import os
import sys
from datetime import timedelta
from pathlib import Path

from backend.core.custom_filter.logging_filter import ExcludeUnnecessaryInformationFilter
from manage import load_env

load_env()

BASE_DIR = Path(__file__).resolve().parent.parent

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv("SECRET_KEY", None)

# SECURITY WARNING: don"t run with debug turned on in production!

DEBUG = bool(int(os.environ.get("DEBUG")))

# SECURITY: For safety reasons. Here should be put only the host names on that  are App will run
ALLOWED_HOSTS = os.getenv("ALLOWED_HOSTS", "").split(" ")

CORS_ALLOW_ALL_ORIGINS = True

# Here should be put the app name.
APPLICATION_NAME = "My Custom Web App Name"

# Application definition
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",

    # Additional Added APPS
    "backend.Accounts",
    "backend.Products",
    "rest_framework",
    "rest_framework_jwt",
    "rest_framework_jwt.blacklist",
    "celery",
    "django_celery_beat",
    "drf_yasg",
    "corsheaders"
]

MIDDLEWARE = [
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "backend.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [BASE_DIR / "templates"]
        ,
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "backend.wsgi.application"

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": os.getenv("DB_NAME"),
        "USER": os.getenv("DB_USER"),
        "PASSWORD": os.getenv("DB_PASSWORD"),
        "HOST": os.getenv("DB_HOST"),
        "PORT": os.getenv("DB_PORT"),
    }
}

# Password validation
# https://docs.djangoproject.com/en/4.2/ref/settings/#auth-password-validators

if DEBUG:
    AUTH_PASSWORD_VALIDATORS = []
else:
    AUTH_PASSWORD_VALIDATORS = [
        {
            "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
        },
        {
            "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
        },
    ]
# Password hasher changed to BCrypt hasher
PASSWORD_HASHERS = [
    "django.contrib.auth.hashers.BCryptPasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]

# Internationalization
# https://docs.djangoproject.com/en/4.2/topics/i18n/

LANGUAGE_CODE = "en-us"

TIME_ZONE = "UTC"

USE_I18N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.2/howto/static-files/

STATIC_URL = "static/"
STATICFILES_DIRS = [
    BASE_DIR / "staticfiles/"
]
STATIC_ROOT = "static/"

# Default primary key field type
# https://docs.djangoproject.com/en/4.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

AUTH_USER_MODEL = "Accounts.JhiUser"

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = os.getenv("EMAIL_HOST")
EMAIL_PORT = os.getenv("EMAIL_PORT")
EMAIL_USE_TLS = bool(int(os.getenv("EMAIL_USE_TLS")))
EMAIL_HOST_USER = os.environ.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = os.environ.get("EMAIL_HOST_PASSWORD")

REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": (
        "rest_framework.permissions.AllowAny",
    ),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "backend.core.authentications.custom_JWT_token_authentication.CustomTokenAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ),
}

LOGIN_REDIRECT_URL = "login"
LOGOUT_REDIRECT_URL = "login"

CELERY_BROKER_URL = os.getenv("CELERY_BROKER_URL")
CELERY_RESULT_BACKEND = os.getenv("CELERY_RESULT_BACKEND")
CELERY_ACCEPT_CONTENT = ["json"]
CELERY_RESULT_SERIALIZER = "json"
CELERY_TASK_SERIALIZER = "json"
CELERY_TIMEZONE = "UTC"
CELERY_BEAT_SCHEDULE = {
    "delete_not_activated_accounts": {
        "task": "DeleteInactiveAccounts",
        "schedule": timedelta(hours=1),
    },
    "delete_expired_reset_password_keys": {
        "task": "DeleteExpiredResetPasswordKeysTask",
        "schedule": timedelta(hours=1),
    },
}

SWAGGER_SETTINGS = {
    "USE_SESSION_AUTH": False,
    "DEFAULT_FIELD_INSPECTORS": [
        "drf_yasg.inspectors.CamelCaseJSONFilter",
        "drf_yasg.inspectors.InlineSerializerInspector",
        "drf_yasg.inspectors.RelatedFieldInspector",
        "drf_yasg.inspectors.ChoiceFieldInspector",
        "drf_yasg.inspectors.FileFieldInspector",
        "drf_yasg.inspectors.DictFieldInspector",
        "drf_yasg.inspectors.SimpleFieldInspector",
        "drf_yasg.inspectors.StringDefaultFieldInspector",
    ],
    "SECURITY_DEFINITIONS": {
        "Basic": {
            "type": "basic"
        }
    }
}

JWT_AUTH = {
    "JWT_REFRESH_HANDLER": "rest_framework_jwt.utils.jwt_payload_handler",
    "JWT_PAYLOAD_GET_USERNAME_HANDLER": "rest_framework_jwt.utils.jwt_get_username_from_payload_handler",
    "JWT_SECRET_KEY": SECRET_KEY,
    "JWT_ALGORITHM": "HS512",
    "JWT_VERIFY": True,
    "JWT_VERIFY_EXPIRATION": True,
    "JWT_LEEWAY": 0,
    "JWT_EXPIRATION_DELTA": timedelta(hours=1),
    "JWT_REFRESH_EXPIRATION_DELTA": timedelta(days=1),
    "JWT_ALLOW_REFRESH": True,
    "JWT_REFRESH_LEEWAY": 0,
    "JWT_AUTH_HEADER_PREFIX": "Bearer",
}

if "test" in sys.argv:
    # Turn off logging the unit and integration tests.
    logging.disable(logging.CRITICAL)

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "{asctime} : {levelname} : {message}",
            "style": "{",
        },
        "verbose": {
            "format": "{asctime} : {levelname} : {message}",
            "style": "{",
        },
    },
    "filters": {
        "exclude_messages": {
            "()": ExcludeUnnecessaryInformationFilter,
        },
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "filters": ["exclude_messages", ],
            "class": "logging.StreamHandler",
            "formatter": "simple",
        },
        "file": {
            "level": "INFO",
            "filters": ["exclude_messages", ],
            "class": "logging.FileHandler",
            "formatter": "simple",
            "filename": BASE_DIR / "log.txt",
        },
    },

    "loggers": {
        "django": {
            "handlers": ["file", "console", ],
            "level": "DEBUG",
            "propagate": True,
        },
    },
}
