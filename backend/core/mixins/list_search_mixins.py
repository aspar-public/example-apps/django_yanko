import logging
from functools import wraps
from importlib import import_module

from rest_framework import status
from rest_framework.response import Response

from backend.core.get_data.get_client_ip import get_client_ip

logger = logging.getLogger('django')


class SearchUserFieldsMixin:
    """
        return: Custom  Mixin with mapped fields for User Model.
    """

    model_objects = "users"

    @property
    def allowed_fields(self):
        __mapped_fields_to_model = {
            "login": "username",
            "firstName": "first_name",
            "lastName": "last_name",
        }
        return __mapped_fields_to_model


class SearchProductFieldsMixin:
    """
        return: Custom  Mixin with mapped fields for
        Product Model and additional operators for Product model fields.
    """
    model_objects = "products"
    product_integer_fields = (
        "id",
        "price",
    )

    @property
    def allowed_fields(self):
        __mapped_fields_to_model = {
            "id": "id",
            "code": "code",
            "name": "name",
            "description": "description",
            "image01ContentType": "image_01_content_type",
            "price": "price",
            "owner": "owner",
        }
        return __mapped_fields_to_model

    @property
    def additional_operators(self):
        __operators = {
            "greaterThan": "__gt",
            "greaterThanOrEqual": "__gte",
            "lessThan": "__lt",
            "lessThanOrEqual": "__lte",
        }
        return __operators


class CustomGetCountDecoratorMixin:
    """
        return: Custom Decorator that modify get method from the mixin and return only length of queryset.
    """

    @staticmethod
    def count_queryset_decorator(get):
        def get_products_count_view():
            """
                return: Prevent Circular Import Error
            """
            return import_module("backend.Products.views").ProductsCountView

        @wraps(get)
        def wrapper(self, request, *args, **kwargs):
            if isinstance(self, get_products_count_view()):
                data = get(self, request, *args, **kwargs)
                if data.status_code == 400:
                    return data

                count = self.get_queryset().count()
                return Response({"count": count}, status=status.HTTP_200_OK)

            return get(self, request, *args, **kwargs)

        return wrapper


class SearchElementsMixin:
    """
        return: Custom Class Mixin for search users and products, return ordered queryset.
    """
    operators_mapping = {
        "contains": "__icontains",
        "doesNotContain": "__not_icontains",
        "equals": "__exact",
        "notEquals": "__not_exact",
        "specified": "__isnull",
        "in": "__in",
        "notIn": "__not_in",
    }

    @CustomGetCountDecoratorMixin.count_queryset_decorator
    def get(self, request, *args, **kwargs):
        client_ip = get_client_ip(request)

        if self.model_objects == "products":
            self.operators_mapping.update(**self.additional_operators)

        for param, value in request.query_params.items():
            if param == "page_size":
                continue

            try:
                field, operator = param.split('.')
            except ValueError:
                logger.info(
                    msg=f"IP:'{client_ip}'. Logged as '{self.request.user.username}' user."
                        f" Try to search with invalid schema " +
                        "{" + f"'field.operator': '{param}', 'value': '{value if value else None}'" + "}."
                )
                return Response(
                    {
                        "message": "Invalid searching schema: " +
                                   "{" + f"'field.operator': '{param}', 'value': '{value if value else None}'" + "}. " +
                                   "Valid searching schemas: [/?field.operator=value], "
                                   "[/?field.operator=value&field2.operator2=value2] !"},
                    status=status.HTTP_400_BAD_REQUEST
                )

            if self.model_objects == "products" and \
                    operator in self.additional_operators.keys() and field not in self.product_integer_fields:
                logger.info(
                    msg=f"IP:'{client_ip}'. Logged as '{self.request.user.username}' user."
                        f" Try to search with invalid operator {operator} for field '{field}'."
                )
                return Response(
                    {
                        "message": f"Attention: {', '.join(f'`{o}`' for o in self.additional_operators.keys())} is valid operators "
                                   f"only for fields: {', '.join(f'`{f}`' for f in self.product_integer_fields)}!"
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
            try:
                field = self.allowed_fields[field]

            except KeyError:
                logger.info(
                    msg=f"IP:'{client_ip}'. Logged as '{self.request.user.username}' user. "
                        f"Try to search with invalid data for field: {field} !"
                )
                return Response(
                    {
                        "message": f"Entered '{field}' is not valid field. "
                                   f"Allowed fields: {', '.join(f'`{f}`' for f in self.allowed_fields.keys())}."},
                    status=status.HTTP_400_BAD_REQUEST)

            try:
                if operator == "specified":
                    if value.capitalize() not in ["True", "False"]:
                        logger.info(
                            msg=f"IP:'{client_ip}'.  Logged as '{self.request.user.username}' user."
                                f" try to search with invalid value for 'specified': {field} !"
                        )
                        return Response(
                            {
                                "message": f"Options for operator 'specified' must be 'True' or 'False'. "
                                           f"You have entered '{value}'."},
                            status=status.HTTP_400_BAD_REQUEST)

                operator = self.operators_mapping[operator]
            except KeyError:
                logger.info(
                    msg=f"IP:'{client_ip}'. Logged as '{self.request.user.username}' user. "
                        f"Try to search with invalid data for operand: '{operator}' !"
                )
                return Response(
                    dict(message=f"Entered '{operator}' is not valid operator. "
                                 f"Allowed operands: {', '.join(f'`{o}`' for o in self.operators_mapping.keys())}!"),
                    status=status.HTTP_400_BAD_REQUEST)

            lookup_expression = f"{field}{operator}"
            request.data.update({lookup_expression: value})

        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        queryset = super().get_queryset()
        client_ip = get_client_ip(self.request)

        for lookup_expression, value in self.request.data.items():
            value = value.split(",") if "," in value else value

            if "__isnull" in lookup_expression:
                value = eval(value.capitalize()) if value.capitalize() in ["False", "True"] else value

            queryset = queryset.filter(**{lookup_expression: value})

        logger.info(
            msg=f"IP:'{client_ip}'. Logged as '{self.request.user.username}' user. "
                f"Proceed search for: {len(queryset)} {self.model_objects}!"
        )
        return queryset.order_by("id")
