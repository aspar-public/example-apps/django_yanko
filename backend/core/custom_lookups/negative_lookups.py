from abc import ABC

from django.db.models import Field
from django.db.models.lookups import In, IContains, Exact


@Field.register_lookup
class NotIContains(IContains, ABC):
    """
        return:  This is custom Lookup Class. Act as '__not_icontains'. Opposite of '__icontains'.
    """
    lookup_name = "not_icontains"

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return "{} NOT ILIKE {}".format(lhs, rhs), params


@Field.register_lookup
class NotEqual(Exact, ABC):
    """
        return:  This is custom Lookup Class. Act as '__not_exact'. Opposite of '__exact'.
    """
    lookup_name = "not_exact"

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '{} <> {}'.format(lhs, rhs), params


@Field.register_lookup
class NotInLookup(In, ABC):
    """
        return: This is custom Lookup Class. Act as '__not_in'. Opposite of '__in'.
    """
    lookup_name = "not_in"

    def as_sql(self, compiler, connection, arg_joiner=','):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = list(lhs_params) + list(rhs_params)
        return "{} NOT IN {}".format(lhs, rhs), tuple(params)
