import random
import string


def user_key_generator(model, field, length):

    while True:
        random_string = ""

        for _ in range(length):
            letter, digit = random.choice(string.ascii_letters), random.choice(string.digits)
            random_string += random.choice((letter, digit))

        if not model.objects.filter(**{field: random_string}).exists():
            return random_string

        # user_key_generator(model, field, length) -> can`t be tested.
