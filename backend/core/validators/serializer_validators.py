from rest_framework import exceptions


def value_is_not_numeric_and_alphabetic(value):
    if not value.isalnum():
        raise exceptions.ValidationError("Key should be only letters and numbers.")
