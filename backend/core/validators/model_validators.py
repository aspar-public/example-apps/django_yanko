from django.core import exceptions


def long_password_validator(value):
    if len(value) > 67:
        raise exceptions.ValidationError(
            f"This password is too Long. It must contain maximum 67 characters, your contains {len(value)}.")
