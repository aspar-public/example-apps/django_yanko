import jwt
from django.apps import apps
from django.contrib.auth import get_user_model
from rest_framework import exceptions
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.blacklist.exceptions import (
    MissingToken,
)
from rest_framework_jwt.compat import ExpiredSignature
from rest_framework_jwt.compat import gettext_lazy as _

UserModel = get_user_model()


class CustomTokenAuthentication(JSONWebTokenAuthentication):
    """
        return: Custom authentication class for User authenticate with modified JWT token
    """

    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        try:
            token = self.get_token_from_request(request)
            if token is None:
                return None
        except MissingToken:
            return None

        try:
            payload = self.jwt_decode_token(token)
        except ExpiredSignature:
            msg = _("Token has expired.")
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = _("Error decoding token.")
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            msg = _("Invalid token.")
            raise exceptions.AuthenticationFailed(msg)

        if apps.is_installed("rest_framework_jwt.blacklist"):
            from rest_framework_jwt.blacklist.models import BlacklistedToken
            if BlacklistedToken.is_blocked(token, payload):
                msg = _("Token is blacklisted.")
                raise exceptions.PermissionDenied(msg)
        try:
            user = UserModel.objects.filter(username=payload["sub"]).get()
        except UserModel.DoesNotExist:
            msg = _("Invalid token.")
            raise exceptions.AuthenticationFailed(msg)
        return user, token
