from django.contrib.auth import get_user_model
from rest_framework.permissions import BasePermission
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

UserModel = get_user_model()


class IsAdminOrOwner(BasePermission):
    """
        return: Custom permission to allow access only to admin users and account owners.
    """

    def has_permission(self, request, view):
        try:
            return len(request.auth) > 1
        except TypeError:
            return request.user.is_authenticated
        except AttributeError:
            return False

    def has_object_permission(self, request, view, obj):
        try:
            token = request.headers["Authorization"].replace("Bearer", "").strip()
            payload = JSONWebTokenAuthentication.jwt_decode_token(token)
            return "ROLE_ADMIN" in payload["auth"] or obj.username == payload["sub"]
        except KeyError:
            return request.user.is_superuser or obj.username == request.user.username
