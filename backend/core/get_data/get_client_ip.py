def get_client_ip(request):
    return getattr(request, "META", {}).get("REMOTE_ADDR", "-")
