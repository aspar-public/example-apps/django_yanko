import logging


class ExcludeUnnecessaryInformationFilter(logging.Filter):
    """
        return: Custom logging filter class that exclude unnecessary information.
    """
    def filter(self, record):
        message = record.getMessage().lower()
        if "file" in message or "watching" in message:
            return False

        return True
