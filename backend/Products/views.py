import logging

from rest_framework import generics as api_views, status, pagination, exceptions, permissions
from rest_framework.response import Response

from .models import ProductModel
from .serializers import ListCreateProductSerializer, ProductsDataActionsSerializer
from ..core.get_data.get_client_ip import get_client_ip
from ..core.mixins.list_search_mixins import SearchProductFieldsMixin, SearchElementsMixin

logger = logging.getLogger('django')


# TODO: Add more test for custom mixins and decorator!
class CustomProductPaginationClass(pagination.PageNumberPagination):
    """
        return: Pagination class  that define the page size(number of objects) of search view.
    """
    page_size = 5
    page_query_param = "page_size"


class ListCreateProductView(SearchProductFieldsMixin, SearchElementsMixin, api_views.ListCreateAPIView):
    """
        return: View  for List and Create products.
        Returns JSON with data for the product or list with products with searching criteria.
        User can use follow operators: 'contains', 'doesNotContain', 'equals', 'notEquals',
        'specified', 'in', 'notIn', 'greaterThan', 'greaterThanOrEqual', 'lessThan', 'lessThanOrEqual'.
    """
    serializer_class = ListCreateProductSerializer
    permission_classes = (
        permissions.IsAuthenticated,
    )
    pagination_class = CustomProductPaginationClass
    queryset = ProductModel.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        client_ip = get_client_ip(self.request)
        if serializer.is_valid():
            logger.info(
                msg=f"IP:'{client_ip}' logged as '{self.request.user.username}', create product. "
                    f"Code: '{serializer.validated_data['code']}', Name: '{serializer.validated_data['name']}'!"
            )
            return super().post(request, *args, **kwargs)

        logger.info(
            msg=f"IP:'{client_ip}' logged as '{self.request.user.username}', try to create product. "
                f"Details: '{serializer.errors}'!")
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ProductsDataActionsView(api_views.RetrieveUpdateDestroyAPIView):
    """
        return: View  for Read, Update, Delete product. Returns JSON with data for the product after update.
    """
    serializer_class = ProductsDataActionsSerializer
    queryset = ProductModel
    permission_classes = (
        permissions.IsAuthenticated,
    )
    lookup_field = "id"

    def get_object(self):
        client_ip = get_client_ip(self.request)
        try:
            return ProductModel.objects.get(**self.kwargs)
        except ProductModel.DoesNotExist:
            logger.info(
                msg=f"IP:'{client_ip}', logged as '{self.request.user.username}' user. "
                    f"Try to get not existing product!"
            )
            raise exceptions.ValidationError("Product not exist.")

    def perform_update(self, serializer):
        product = self.get_object()
        client_ip = get_client_ip(self.request)
        logger.info(
            msg=f"IP:'{client_ip}', logged as '{self.request.user.username}' user. "
                f"Has updated product - id: {product.id}, code: '{product.code}', name: '{product.name}'!"
        )
        serializer.save()


class ProductsCountView(ListCreateProductView):
    """
        return: View  for List Count products. Returns JSON with data count for products.
    """
    http_method_names = ("get",)
