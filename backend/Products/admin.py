from django.contrib import admin

from .models import ProductModel


@admin.register(ProductModel)
class AdminProduct(admin.ModelAdmin):
    ordering = (
        "id",
        "code",
        "name",
    )
    list_display = (
        "id",
        "code",
        "name",
        "image_01_content_type",
        "owner",
    )
    search_fields = (
        "code",
        "name",
        "owner",
    )
    fieldsets = (
        ("Main information", {
            "fields": (
                "id",
                ("code", "name",),
                "owner"
            ),
        }),
        ("About the Product", {
            "fields": (
                "description",
                ("image_01", "image_01_content_type"),
            ),
        }),
    )
    readonly_fields = (
        "id",
    )
