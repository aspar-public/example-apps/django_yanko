from django.contrib.auth import get_user_model
from django.db import models
from django_base64field import fields as base_64_field

UserModel = get_user_model()


class ProductModel(models.Model):
    code = models.CharField(
        unique=True,
    )
    name = models.CharField()
    description = models.CharField(
        null=True,
        blank=True,
    )
    price = models.DecimalField(
        null=True,
        blank=True,
        max_digits=21,
        decimal_places=2,
    )
    image_01 = base_64_field.Base64Field(
        null=True,
        blank=True
    )
    image_01_content_type = models.CharField(
        null=True,
        blank=True,
    )
    owner = models.ForeignKey(
        UserModel,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"id: {self.id}, code: {self.code}, name: {self.name}."
