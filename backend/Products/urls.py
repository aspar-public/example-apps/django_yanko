from django.urls import path

from .views import ListCreateProductView, ProductsCountView, ProductsDataActionsView

urlpatterns = [
    path("", ListCreateProductView.as_view(), name="list create product"),
    path("<int:id>/", ProductsDataActionsView.as_view(), name="product data actions"),
    path("count/", ProductsCountView.as_view(), name="products count"),
]
