from django.contrib.auth import get_user_model
from rest_framework import serializers, validators as drf_validators

from backend.Products.models import ProductModel

UserModel = get_user_model()


class ListCreateProductSerializer(serializers.ModelSerializer):
    """
        return: Class Serializer for List And Create products.
    """
    code = serializers.CharField(
        required=True,
        validators=(
            drf_validators.UniqueValidator(ProductModel.objects.all(), "Product with that code exist."),
        ),
    )
    name = serializers.CharField(
        required=True
    )
    description = serializers.CharField(
        required=False,
        allow_blank=True
    )
    price = serializers.DecimalField(
        required=False,
        allow_null=True,
        max_digits=21,
        decimal_places=2,
    )
    image01 = serializers.CharField(
        source="image_01",
        required=False,
        allow_blank=True,
    )
    image01ContentType = serializers.CharField(
        source="image_01_content_type",
        required=False,
        allow_blank=True,
    )
    owner = serializers.PrimaryKeyRelatedField(
        queryset=UserModel.objects.all(),
        required=False,
        allow_null=True,
    )

    class Meta:
        model = ProductModel
        fields = (
            "code",
            "name",
            "description",
            "price",
            "image01",
            "image01ContentType",
            "owner",
        )

    def to_representation(self, instance):
        super().to_representation(instance)

        represented_fields = {
            "id": instance.id,
            "code": instance.code,
            "name": instance.name,
            "description": instance.description,
            "price": instance.price,
            "image01": [
                instance.image_01
            ],
            "image01ContentType": instance.image_01_content_type,
            "owner": {
                "id": instance.owner.id if instance.owner else "null",
                "login": instance.owner.username if instance.owner else "null",
            }
        }

        return represented_fields


class ProductsDataActionsSerializer(ListCreateProductSerializer):
    class Meta:
        model = ProductModel
        fields = (
            "code",
            "name",
            "description",
            "price",
            "image01",
            "image01ContentType",
        )
