from django.urls import path, include

from .views import CreateJhiUserView, LoginJhiUserView, SearchJhiUserView, \
    ActivateAccountJhiUserView, ChangePasswordView, JhiUserDataActionsView, PasswordResetRequestView, \
    PasswordResetFinishView  # , TestVIew  # TODO: Delete TestView

urlpatterns = [
    path("register/", CreateJhiUserView.as_view(), name="register"),
    path("authenticate/", LoginJhiUserView.as_view(), name="login"),
    path("activate?key=<str:key>", ActivateAccountJhiUserView.as_view(), name="account activation"),
    path("account/", include([
        path("", JhiUserDataActionsView.as_view(), name="user data"),
        path("change-password/", ChangePasswordView.as_view(), name="change password"),
        path("reset-password/", include([
            path("init/", PasswordResetRequestView.as_view(), name="reset password request"),
            path("finish/reset?key=<str:key>", PasswordResetFinishView.as_view(), name="reset password finish"),
        ])),
    ])),
    path("admin/users", SearchJhiUserView.as_view(), name="search users", ),
    # path("", TestVIew.as_view(), name="home"),  # TODO: Delete this URL in final phase.
]
