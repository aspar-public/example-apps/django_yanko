from django.db.models.signals import post_save
from django.dispatch import receiver

from backend.Accounts.emails import send_activation_email
from backend.Accounts.models import JhiUser


@receiver(post_save, sender=JhiUser)
def user_created(instance, created, **kwargs):
    if created and not instance.is_superuser:
        send_activation_email(instance)
