from django.contrib.auth import models as auth_models
from django.contrib.auth.password_validation import validate_password
from django.db import models

from ..core.generators.user_key_generator import user_key_generator
from ..core.validators.model_validators import long_password_validator


class JhiUser(auth_models.AbstractUser):
    LANGUAGE_CHOICES = (
        ("en", "English"),
        ("bg", "Bulgarian"),
        ("fr", "French"),
        ("de", "German"),
    )

    password = models.CharField(
        null=True,
        blank=True,
        validators=(validate_password, long_password_validator)
    )
    username = models.CharField(
        max_length=50,
        unique=True,
    )
    first_name = models.CharField(
        null=True,
        blank=True,
        max_length=50,
    )
    last_name = models.CharField(
        null=True,
        blank=True,
        max_length=50,
    )
    email = models.EmailField(
        unique=True,
        max_length=191,
    )
    image_url = models.URLField(
        null=True,
        blank=True,
        max_length=256,
    )
    is_active = models.BooleanField(
        default=False,
    )
    lang_key = models.CharField(
        null=True,
        blank=True,
        max_length=10,
        choices=LANGUAGE_CHOICES,
    )
    activation_key = models.CharField(
        null=True,
        blank=True,
        max_length=20,
    )
    reset_key = models.CharField(
        null=True,
        blank=True,
        max_length=20,
    )
    reset_day = models.DateTimeField(
        null=True,
        blank=True,
    )
    created_by = models.CharField(
        max_length=50,
        default="anonymousUser"
    )
    last_modified_by = models.CharField(
        max_length=50,
        default="anonymousUser"
    )
    last_modified_date = models.DateTimeField(
        auto_now=True,
    )

    def save(self, *args, **kwargs):

        if "bcrypt" not in self.password:
            self.set_password(self.password)

        if not self.activation_key:
            self.activation_key = user_key_generator(JhiUser, "activation_key", 20)

        if self.is_superuser:  # !!! is_superuser = "ROLE_ADMIN" == full CRUD rights
            self.is_active = True

        super(JhiUser, self).save(*args, **kwargs)
