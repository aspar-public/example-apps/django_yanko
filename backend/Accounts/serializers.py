import logging
from datetime import datetime

import jwt
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.password_validation import validate_password
from django.core import validators
from rest_framework import serializers, validators as drf_validators
from rest_framework_jwt import serializers as auth_serializer

from backend import settings
from backend.Accounts.helpers import find_roles_of_user
from backend.core.get_data.get_client_ip import get_client_ip
from backend.core.validators.serializer_validators import value_is_not_numeric_and_alphabetic

UserModel = get_user_model()

logger = logging.getLogger("django")


class CustomToRepresentationMixin:
    """
        return: Custom Class Mixin to arrange JSON representation.
    """

    def to_representation(self, instance):
        super().to_representation(instance)

        represented_fields = {
            "id": instance.id,
            "login": instance.username,
            "firstName": instance.first_name,
            "lastName": instance.last_name,
            "email": instance.email,
            "imageUrl": instance.image_url,
            "activated": instance.is_active,
            "langKey": instance.lang_key,
            "createdBy": instance.created_by,
            "createdDate": instance.date_joined,
            "lastModifiedBy": instance.last_modified_by,
            "lastModifiedDate": instance.last_modified_date,
            "authorities": find_roles_of_user(instance)
        }

        return represented_fields


class JhiUserCreateSerializer(CustomToRepresentationMixin, serializers.ModelSerializer):
    """
        return:  Serializer for user Creation/Registration. Return user data.
    """
    login = serializers.CharField(
        source="username",
        required=False,
        validators=(
            drf_validators.UniqueValidator(UserModel.objects.all(), "User with that login exist."),
        ),
        max_length=50,
    )
    firstName = serializers.CharField(
        source="first_name",
        required=True,
        allow_blank=True,
    )
    lastName = serializers.CharField(
        source="last_name",
        required=True,
        allow_blank=True,
    )
    imageUrl = serializers.CharField(
        source="image_url",
        required=False,
        allow_blank=True,
    )
    langKey = serializers.ChoiceField(
        source="lang_key",
        required=False,
        choices=UserModel.LANGUAGE_CHOICES,
    )

    class Meta:
        model = UserModel
        fields = (
            "login",
            "firstName",
            "lastName",
            "email",
            "imageUrl",
            "langKey",
            "password",
        )
        extra_kwargs = {
            "password": {"write_only": True},
        }


class JhiUserActivationAccountSerializer(CustomToRepresentationMixin, serializers.ModelSerializer):
    """
        return:  Serializer for user Activation User Account.
    """

    class Meta:
        model = UserModel
        fields = "__all__"


class JhiUserChangePasswordSerializer(serializers.ModelSerializer):
    """
        return: Serializer for User change password.
    """
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    old_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = UserModel
        fields = (
            "old_password",
            "password",
            "password2",
        )

    def validate(self, attrs):
        if attrs["password"] != attrs["password2"]:
            raise serializers.ValidationError({"password": "Password fields didn't match."})

        return attrs

    def validate_old_password(self, value):
        user = self.context["request"].user
        if not user.check_password(value):
            raise serializers.ValidationError({"old_password": "Old password is not correct"})
        return value

    def update(self, instance, validated_data):

        instance.set_password(validated_data["password"])
        instance.save()

        return instance


class JhiUserDataActionsSerializer(CustomToRepresentationMixin, serializers.ModelSerializer):
    """
        return: Serializer for User Details, Update, Delete actions.
    """

    id = serializers.IntegerField(
        required=False,
        read_only=True,
    )
    login = serializers.CharField(
        source="username",
        required=False,
        read_only=True,
    )
    firstName = serializers.CharField(
        source="first_name",
        required=False,
        allow_blank=True,
    )
    lastName = serializers.CharField(
        source="last_name",
        required=False,
        allow_blank=True,
    )
    email = serializers.EmailField(
        required=False,
        allow_blank=False,
    )
    imageUrl = serializers.CharField(
        source="image_url",
        required=False,
        allow_blank=True,
    )
    activated = serializers.BooleanField(
        source="is_active",
        read_only=True,
        required=False,
    )
    langKey = serializers.ChoiceField(
        source="lang_key",
        required=False,
        choices=UserModel.LANGUAGE_CHOICES,
    )
    createdBy = serializers.CharField(
        source="created_by",
        required=False,
        read_only=True,
    )
    createdDate = serializers.DateTimeField(
        source="date_joined",
        required=False,
        read_only=True,
    )
    lastModifiedBy = serializers.CharField(
        source="last_modified_by",
        required=False,
        read_only=True,
    )
    lastModifiedDate = serializers.DateTimeField(
        source="last_modified_date",
        required=False,
        read_only=True,
    )

    class Meta:
        model = UserModel
        fields = (
            "id",
            "login",
            "firstName",
            "lastName",
            "email",
            "imageUrl",
            "activated",
            "langKey",
            "createdBy",
            "createdDate",
            "lastModifiedBy",
            "lastModifiedDate",
        )


class JhiUserResetPasswordRequestSerializer(serializers.Serializer):
    """
        return: Serializer for request password reset.
    """
    email = serializers.EmailField()

    def validate(self, attrs):
        try:
            user = UserModel.objects.filter(email=attrs.get("email")).get()

            return user

        except UserModel.DoesNotExist:
            raise serializers.ValidationError(
                "Email does not exist. Please contact Administrator or check your email is correct.")


class JhiUserResetPasswordFinishSerializer(serializers.Serializer):
    """
         return: Serializer for finish password reset.
     """
    new_password = serializers.CharField(
        max_length=60,
        required=True,
        validators=(
            validate_password,
        ),
    )
    key = serializers.CharField(
        source="reset_key",
        max_length=20,
        required=True,
        validators=(
            validators.MinLengthValidator(
                20,
                message=f"Ensure this key have exactly 20 characters. Yous is (%(show_value)d)."
            ),
            value_is_not_numeric_and_alphabetic,
        )
    )

    class Meta:
        fields = (
            "new_password",
            "key",
        )

    def validate(self, attrs):
        def not_valid_key_error():
            raise serializers.ValidationError("Not valid key! Please, make new request for key!")

        reset_key = self.context

        if reset_key != attrs.get("reset_key") and UserModel.objects.filter(reset_key=reset_key).count() == 1:
            raise serializers.ValidationError("Please copy again key from the email!")

        if not reset_key or len(reset_key) != 20 or not reset_key.isalnum():
            not_valid_key_error()

        try:
            if reset_key:
                user = UserModel.objects.filter(reset_key=reset_key).get()
                return user

        except UserModel.DoesNotExist:
            not_valid_key_error()


class JhiUserLoginSerializer(auth_serializer.JSONWebTokenSerializer):
    """
         return: Serializer for login. Obtain JWT Token with Option to increase life of token to 24 hours.
     """
    rememberMe = serializers.BooleanField(default=False, label="Remember me")

    @staticmethod
    def custom_jwt_payload_handler(user):
        custom_payload = {
            "sub": user.get_username(),
            "auth": ",".join(find_roles_of_user(user)[::-1]),
            "exp": datetime.utcnow() + settings.JWT_AUTH["JWT_EXPIRATION_DELTA"],
        }

        return custom_payload

    def validate(self, data):
        credentials = {
            self.username_field: data.get(self.username_field),
            "password": data.get("password")
        }

        request = self.context["request"]
        user = authenticate(request, **credentials)

        client_ip = get_client_ip(request)

        if not user:
            logger.info(
                msg=f"IP: '{client_ip}' try to authenticate as '{credentials[self.username_field]}'.")
            raise serializers.ValidationError("Unable to authenticate with provided credentials.")

        data = super(JhiUserLoginSerializer, self).validate(data)
        remember_me = self.initial_data.get("rememberMe", False)

        payload = self.custom_jwt_payload_handler(user)

        if remember_me and "token" in data:
            expiration = settings.JWT_AUTH["JWT_REFRESH_EXPIRATION_DELTA"]
            payload["exp"] = datetime.utcnow() + expiration

        token = jwt.encode(payload, settings.SECRET_KEY, algorithm='HS512', headers={"alg": "HS512"})

        data["remember_me"] = remember_me
        data["token"] = str(token)[2:-1]
        data["token_exp"] = payload["exp"]

        logger.info(
            msg=f"IP: '{client_ip}' successfully authenticated as '{user.username}'."
                f" Remember me: '{str(remember_me).capitalize()}'.")

        return data

    def to_representation(self, instance):
        representation = {
            "id_token": instance["token"],
            "user_id": instance["user"].id,
            "username": instance["user"].username,
            "rememberMe": instance["remember_me"],
            "expriesAt": datetime.utcfromtimestamp(instance["token_exp"]),
        }

        return representation


class JhiUserSearchSerializer(CustomToRepresentationMixin, serializers.Serializer):
    """
        return: Serializer for Search "Form" for RestAPI requests.
    """

    class Meta:
        model = UserModel
        exclude = "__all__"
