from __future__ import absolute_import, unicode_literals

import logging
from datetime import timedelta, datetime

from celery import shared_task
from django.contrib.auth import get_user_model

from backend.Accounts.helpers import clear_reset_password_data

UserModel = get_user_model()

logger = logging.getLogger('django')


def time_limit():
    EXPIRATION_TIME = 24  # Hours
    return datetime.now() - timedelta(hours=EXPIRATION_TIME)


@shared_task(name="DeleteInactiveAccounts")
def delete_not_activated_account():
    inactive_users = UserModel.objects.filter(is_active=False, date_joined__lt=time_limit()).all()

    if inactive_users:
        for user in inactive_users:
            username = user.username

            user.delete()
            logger.info(msg=f"User deleted: {username}")

        return f"Founded {len(inactive_users)} not activated users are deleted."
    else:
        logger.info("No users found for deletion.")
        return "Users not found. No actions are taken."


@shared_task(name="DeleteExpiredResetPasswordKeysTask")
def delete_expired_password_reset_keys():
    users_with_expired_reset_keys = UserModel.objects.filter(reset_day__lt=time_limit()).all()

    if users_with_expired_reset_keys:
        for user in users_with_expired_reset_keys:
            clear_reset_password_data(user)

            logger.info(msg=f"Key deleted for user:{user.username}")

        return f"Founded {len(users_with_expired_reset_keys)} users with expired keys. Keys are deleted."
    else:
        logger.info(msg="No expired keys found.")
        return "Users with expired reset keys are not found. No actions are taken."
