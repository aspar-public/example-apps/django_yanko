import logging

from django.contrib import admin
from django.contrib.auth import get_user_model

from backend.Accounts.helpers import update_user_modify_by_fields
from backend.core.get_data.get_client_ip import get_client_ip

UserModel = get_user_model()
logger = logging.getLogger("django")

@admin.register(UserModel)
class AdminJhi(admin.ModelAdmin):
    ordering = (
        "date_joined",
        "username",
        "first_name",
        "last_name",
    )
    list_display = (
        "username",
        "first_name",
        "last_name",
        "is_superuser",
        "is_active",
        "date_joined",
    )
    search_fields = (
        "username",
        "first_name",
        "last_name",
    )
    fieldsets = (
        ("Main information", {
            "fields": (
                "username",
                "password",
                "email",
            ),
        }),
        ("Personal Information", {
            "fields": (
                ("first_name", "last_name"),
                "image_url",
                "lang_key",
            ),
        }),
        ("System Information", {
            "fields": (
                ("is_active", "date_joined", "last_login"),
                ("is_superuser", "is_staff"),
                ("activation_key", "reset_key"),
                ("created_by", "last_modified_by"),
                ("reset_day", "last_modified_date"),
                "groups",
                "user_permissions",
            ),
        }),
    )
    readonly_fields = (
        "last_modified_date",
        "date_joined",
        "last_login",
    )

    def save_model(self, request, user_object, form, change):
        client_ip = get_client_ip(request)

        if change and "bcrypt" not in user_object.password:
            new_password = form.cleaned_data.get("password")
            user_object.set_password(new_password)

        logger.info(msg=f"IP:'{client_ip}', admin update user: '{user_object.username}'! Changed fields: {form.changed_data}")
        update_user_modify_by_fields(user_object)

