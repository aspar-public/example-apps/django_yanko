from datetime import datetime


def update_user_modify_by_fields(object_user, executor="admin"):
    object_user.last_modified_by = executor
    object_user.last_modified_date = datetime.now()
    object_user.save()


def clear_reset_password_data(user_object, extra_field="admin"):
    user_object.reset_date = None
    user_object.reset_key = None
    update_user_modify_by_fields(user_object, extra_field)


def find_roles_of_user(instance):
    return ["ROLE_USER"] if not instance.is_superuser else ["ROLE_USER", "ROLE_ADMIN"]
