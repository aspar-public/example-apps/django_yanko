import logging
from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import redirect
from rest_framework import generics as api_views, status, permissions, pagination
from rest_framework.response import Response
from rest_framework_jwt import views as auth_jwt

from .emails import send_reset_password_email
from .helpers import update_user_modify_by_fields, clear_reset_password_data
from .serializers import JhiUserCreateSerializer, JhiUserChangePasswordSerializer, JhiUserDataActionsSerializer, \
    JhiUserActivationAccountSerializer, JhiUserResetPasswordRequestSerializer, JhiUserResetPasswordFinishSerializer, \
    JhiUserLoginSerializer, JhiUserSearchSerializer
from ..core.generators.user_key_generator import user_key_generator
from ..core.get_data.get_client_ip import get_client_ip
from ..core.mixins.list_search_mixins import SearchUserFieldsMixin, SearchElementsMixin
from ..core.permissions.permissions import IsAdminOrOwner

UserModel = get_user_model()
logger = logging.getLogger('django')


# from django import forms
# from .models import JhiUser
# from rest_framework.reverse import reverse_lazy
# from django.views.generic import CreateView
#
# class TestForm2(forms.ModelForm):
#     class Meta:
#         model = UserModel
#         fields = ["username", "first_name", "last_name", "email", "image_url", "lang_key", "password"]
#
#
# class TestVIew(CreateView):
#     """
#         return: Temporary Test view with django generic Views.
#     """
#
#     template_name = "test.html"
#     model = JhiUser
#
#     form_class = TestForm2
#     success_url = reverse_lazy("home")
#
#     @staticmethod
#     def seed_db():
#         """
#             return: Only for development purpose, to seed DB with users on home page View.
#         """
#         from backend.Accounts.models import JhiUser
#
#         users_names = [
#             "Atanas Atanasov",
#             "Atanas Georgiev",
#             "Atanas Ivanov",
#             "Atanas Stamatov",
#             "Georgi Atanasov",
#             "Georgi Georgiev",
#             "Georgi Ivanov",
#             "Georgi Stamatov",
#             "Ivan Atanasov",
#             "Ivan Georgiev",
#             "Ivan Ivanov",
#             "Ivan Stamatov",
#             "Stamat Atanasov",
#             "Stamat Georgiev",
#             "Stamat Ivanov",
#             "Stamat Stamatov",
#         ]
#
#         for name in users_names:
#             id_num = len(UserModel.objects.all())
#             first_name, last_name = name.split()
#             username = f"{first_name.lower()}{id_num}"
#             JhiUser.objects.create_user(
#                 username=username,
#                 first_name=first_name,
#                 last_name=last_name,
#                 email=f"no.reply.mymotomadness+{id_num}@gmail.com",
#                 is_active=True,
#                 is_superuser=True if id_num % 2 == 0 else False,
#                 is_staff=True if id_num % 2 == 0 else False,
#                 password="1234",
#             )
#
#     def get(self, request, *args, **kwargs):
#         self.seed_db()
#         return super().get(request, args, kwargs)


class CreateJhiUserView(api_views.CreateAPIView):
    """
        return: View  for user registration. Returns JSON with data for the user.
    """
    serializer_class = JhiUserCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        client_ip = get_client_ip(request)
        if not serializer.is_valid():
            logger.info(msg=f"IP:'{client_ip}', try registration. Details: '{serializer.errors}'!")
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        logger.info(
            msg=f"IP:'{client_ip}', registered user - id: "
                f"{UserModel.objects.all().last().id if UserModel.objects.all() else 0 + 1}, "
                f"username: '{serializer.validated_data['username']}'!"
        )
        return super().post(request, *args, **kwargs)


class LoginJhiUserView(auth_jwt.ObtainJSONWebTokenView):
    """
        return: View for Login User via ["username", "password"]. Return JWT token.
    """
    serializer_class = JhiUserLoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return super().post(request, *args, **kwargs)


class CustomUserPaginationClass(pagination.PageNumberPagination):
    """
        return: Pagination class  that define the page size(number of objects) of search view.
    """
    page_size = 3
    page_query_param = "page_size"


class SearchJhiUserView(SearchUserFieldsMixin, SearchElementsMixin, api_views.ListCreateAPIView):
    """
       return: Class for user search by ['username', 'first_name, 'last_name'] fields.
        User can use follow operators: 'contains', 'doesNotContain', 'equals', 'notEquals', 'specified', 'in', 'notIn'.
        Accessible only for Admin users.
    """
    serializer_class = JhiUserSearchSerializer
    pagination_class = CustomUserPaginationClass
    permission_classes = (
        permissions.IsAdminUser,
    )
    queryset = UserModel.objects.all()
    http_method_names = ("get",)


class ActivateAccountJhiUserView(api_views.views.APIView):
    """
        return: View for User Account  Activation. Link is sent on Email.
    """
    serializer_class = JhiUserActivationAccountSerializer

    def get(self, request, *args, **kwargs):
        client_ip = get_client_ip(request)
        try:
            user = UserModel.objects.filter(activation_key=kwargs["key"]).get()

            if user.is_active:
                logger.info(msg=f"IP:'{client_ip}', try to activate already activated account '{user.username}'.")
                return redirect("login")

            user.is_active = True
            update_user_modify_by_fields(user)

            logger.info(msg=f"IP:'{client_ip}', activate successfully - id: {user.id}, username: '{user.username}'!")
            return Response(
                self.serializer_class(user).data,
                status=status.HTTP_202_ACCEPTED,
            )
        except UserModel.DoesNotExist:
            logger.info(msg=f"IP:'{client_ip}', try to activate account with invalid key '{kwargs['key']}'.")
            return Response(
                {
                    "error": "Key does not exist. Please contact with Administrator."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )


class ChangePasswordView(api_views.UpdateAPIView):
    """
        return: View for User Change password.
    """
    queryset = UserModel
    serializer_class = JhiUserChangePasswordSerializer
    permission_classes = (
        permissions.IsAuthenticated,
        IsAdminOrOwner,
    )

    def get_object(self):
        return self.request.user


class JhiUserDataActionsView(api_views.RetrieveUpdateDestroyAPIView):
    """
        return: View for User Details, Update, Delete.
    """
    queryset = UserModel
    serializer_class = JhiUserDataActionsSerializer
    permission_classes = (
        IsAdminOrOwner,
    )

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        update_user_modify_by_fields(self.get_object(), executor=self.request.user.username)
        user = self.get_object()
        client_ip = get_client_ip(self.request)
        logger.info(
            msg=f"IP:'{client_ip}', logged as '{self.request.user.username}' user. Has updated user - id: {user.id}, username: '{user.username}'!")
        serializer.save()


class PasswordResetRequestView(api_views.views.APIView):
    """
        return: View for password reset request.
    """

    serializer_class = JhiUserResetPasswordRequestSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        client_ip = get_client_ip(request)
        if serializer.is_valid():
            user = serializer.validated_data

            user.reset_key = user_key_generator(UserModel, "reset_key", 20)
            user.reset_date = datetime.now()
            update_user_modify_by_fields(user)

            send_reset_password_email(user, get_current_site(request))

            logger.info(msg=f"IP: '{client_ip}', sent email request for reset password on '{user.username}'.")
            return Response(
                {
                    "message": "Reset password request was successful. Check your email for details."
                },
                status=status.HTTP_200_OK
            )

        logger.info(msg=f"IP:'{client_ip}', try to request password reset. Details: '{serializer.errors}'!")
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class PasswordResetFinishView(api_views.views.APIView):
    """
        return: View to finish Reset password procedure.
    """
    serializer_class = JhiUserResetPasswordFinishSerializer

    def get(self, request, *args, **kwargs):
        try:
            UserModel.objects.filter(reset_key=self.kwargs["key"]).get()
            return Response(
                {"message": "Enter your new password"},
                status=status.HTTP_200_OK
            )
        except UserModel.DoesNotExist:
            client_ip = get_client_ip(request)

            logger.info(msg=f"IP:'{client_ip}', try to access Reset Password View with invalid key '{kwargs['key']}'.")
            return Response(
                {"message": "Reset key is not valid. Check your link again or make request for new key."},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context=kwargs["key"])
        client_ip = get_client_ip(request)

        if serializer.is_valid():
            user = serializer.validated_data
            user.set_password(request.data.get("new_password"))
            clear_reset_password_data(user, extra_field=f"{user.username}")

            logger.info(msg=f"IP:'{client_ip}', successfully reset password for user '{user.username}'.")
            return Response(
                {
                    "message": "Password reset successful."
                },
                status=status.HTTP_200_OK
            )

        logger.info(msg=f"IP:'{client_ip}', try to reset password. Details: {serializer.errors}.")
        return Response(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )
