from backend import settings
from backend.core.email_utils.send_email import send_email_to_recipient


def send_activation_email(user_object, domain_name="http://127.0.0.1:8000/"):
    context = {
        "user": user_object,
        "domain_name": domain_name,
        "app_name": settings.APPLICATION_NAME
    }

    return send_email_to_recipient(
        subject="Account activation!",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=(user_object.email,),
        template_name="account_activation.html",
        context=context,
    )


def send_reset_password_email(user_object, domain_name):
    context = {
        "user": user_object,
        "domain_name": domain_name,
        "app_name": settings.APPLICATION_NAME,
    }
    return send_email_to_recipient(
        subject="Reset password!",
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=(user_object.email,),
        template_name="password_reset.html",
        context=context,
    )
