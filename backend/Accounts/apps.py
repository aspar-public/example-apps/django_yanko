from django.apps import AppConfig


class AccountsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "backend.Accounts"

    def ready(self):
        import backend.Accounts.signals
        result = super().ready()
        return result