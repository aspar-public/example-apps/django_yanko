FROM python:3.9-slim-buster AS base

WORKDIR /app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=backend.settings
# @Trifon define which variables are passed to this container at build time.

ENV DB_HOST $DB_HOST

FROM base AS builder

COPY requirements.txt /app/

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY staticfiles /app/.
COPY . .

FROM builder AS preparations

RUN python manage.py makemigrations
#RUN python manage.py migrate
#
#RUN echo "from backend.Accounts.models import JhiUser as User; \
#    from envs.superuser import DJANGO_SUPERUSER_USERNAME; \
#    from envs.superuser import DJANGO_SUPERUSER_EMAIL; \
#    from envs.superuser import DJANGO_SUPERUSER_PASSWORD; \
#    User.objects.create_superuser(DJANGO_SUPERUSER_USERNAME,DJANGO_SUPERUSER_EMAIL,DJANGO_SUPERUSER_PASSWORD)" \
#    | python manage.py shell


FROM preparations AS final

EXPOSE 80
#CMD ["sh", "-c", "python manage.py runserver"]
CMD ["sh", "-c", "gunicorn --bind 0.0.0.0:81 backend.wsgi:application"]
#CMD ["sh", "-c", "gunicorn --bind 0.0.0.0:8000 backend.wsgi:application & exec celery -A backend worker -l info -B"]
