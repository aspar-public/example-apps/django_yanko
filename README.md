# django_yanko

___________________________________________________________________

## Project Technologies

- [ ] Python/Django Restframework
- [ ] PostgreSQL
- [ ] Celery
- [ ] Redis
- [ ] Bcrypt
- [ ] JWT Token Authentication
- [ ] Base64fields
- [ ] Docker
- [ ] GitLab CI/CD workflow - Kubernetes

___________________________________________________________________

## Django Rest Api Web App

- Users
- Registrations
- Roles
- Permissions/Authentications
- Custom Django Admin Panel
- Logging
- Bcrypt password hashing
- Custom JWT Token Payload
- Account activation with email confirmation.
- Scheduled Celery workers: Remove not activated accounts. Remove reset keys that are not used.
- Pagination, filtering, search, custom lookups expressions.
- Products - Create, Read, Update, Delete. Search functionality by multiple criteria.
- Reset password using email confirmation.

___________________________________________________________________

## Access

### Default HOST and PORT

- [ ] http://localhost:8000

- [ ] http://127.0.0.1:8000

### App URLs to access different functionalities

#### Users

- User registration.
  - [ ] http:// your-host /api/register/
- User authentication
    - [ ] http:// your-host /api/authenticate/
- User account activation using E-mail.
    - [ ] http:// your-host /api/activate?key=activation-key 
- User Account Update, Read, Delete.
    - [ ] http:// your-host /api/account/
- User change password.
    - [ ] http:// your-host /api/account/change-password/
- User change password using E-mail - request.
    - [ ] http:// your-host /api/account/reset-password/init/
- User change password using E-mail - finish procedure.
    - [ ] http:// your-host /api/account/reset-password/finish/reset?key=reset-key
- Search view accessible only for Admin Users. Search Users by model fields.
    - [ ] http:// your-host /api/admin/users<?field.operator=value.....>
- Custom Admin panel.
    - [ ] http:// your-host /admin/

### Products

- Create product. Use POST request.
    - [ ]  http:// your-host /products/
- Search product view with multiple search criteria. Use GET request.
    - [ ] http:// your-host /products/
- Product Read, Update, Delete.
    - [ ] http:// your-host /products/product-id/
- Return count of searched products.
    - [ ] http:// your-host /products/count/

___________________________________________________________________

## Start project

### Clone repo

```
git clone https://gitlab.com/aspar-public/example-apps/django_yanko.git
```

### Create virtual environment

- Linux/Mac

```
python -m venv venv/bin/activate
```

- Windows

```
python -m venv venv\Scripts\activate
```

### Activate virtual environment

```
python -m venv venv
```

### Upgrade pip

```
python -m pip install --upgrade pip
```

### Install dependencies

```
pip install -r requirements.txt
```

### You need to have running PostgreSQL DB and Redis.

- set up DB with docker command or you can use docker-compose.yml file, provide your credentials in envs/db.env.

```
docker run -d --name postgres -p 5432:5432  --network testnetwork -e POSTGRES_PASSWORD=your-pasword -e POSTGRES_USER=your-username -e POSTGRES_DB=your-db-name postgres:latest
```

- Setup redis server with docker-compose.yml - Needed to start Celery. Provide your redis host name in django_yanko.env.
  files.

### Migrate models to DB.

```
python manage.py makemigrations
```

```
python manage.py migrate
```

### Run App

```
python manage.py runserver
```

### Start Celery Worker and Beat

#### ~~~ In Separate Terminals! ~~~

- Linux

```
celery -A backend worker -l info -B
```

- Windows

```
celery -A backend worker -l info
```

```
celery -A backend beat -l info
```

### Alternative is to use docker-compose.yml configuration and to build it in container.

### ~~~ Provide your credentials in env folder. ~~~

```
docker-compose up -d --build
```

___________________________________________________________________

## To create super user / Admin

```
python manage.py createsuperuser
```

#### NEED TO ENTER

- Enter your username
- Enter your email
- Double input your password

________________________________________________________________

## To Run unit and integration tests

Run tests

```
python manage.py test
```

### Check Tests coverage

Start coverage tests check

```
coverage run manage.py test
```

Extract coverage tests report

```
coverage report > tests_report.txt
```

___________________________________________________________________