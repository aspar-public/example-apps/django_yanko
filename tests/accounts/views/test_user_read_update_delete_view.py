from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

UserModel = get_user_model()


class JhiUserDataActionsViewTests(TestCase):
    """
        return: Test JhiUserDataActionsView, Retrieve, Update, Delete actions with additional invalid data cases.
    """

    def setUp(self):
        self.client = APIClient()
        # Create a user for testing
        self.user = UserModel.objects.create_user(
            username="testuser",
            first_name="Test",
            last_name="User",
            email="testuser@example.com",
            password="password123",
            is_active=True,
        )
        self.url = reverse("user data")
        #self.url = reverse("user data", kwargs={"pk": self.user.pk})

    def test_update_user_with_valid_data_successfully(self):
        valid_data = {
            "username": "testuser",
            "firstName": "UpdatedFirst",
            "lastName": "UpdatedLast",
            "email": "updated@example.com",
            "langKey": "en",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, valid_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK, response.content)
        self.user.refresh_from_db()
        self.assertEqual(self.user.first_name, valid_data["firstName"])
        self.assertEqual(self.user.last_name, valid_data["lastName"])
        self.assertEqual(self.user.email, valid_data["email"])
        self.assertEqual(self.user.lang_key, valid_data["langKey"])

    def test_update_user_with_empty_email_field(self):
        invalid_data = {"email": ""}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, invalid_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        error_message = "This field may not be blank."
        self.assertIn(error_message, response.data["email"])

    def test_update_user_with_invalid_email(self):
        invalid_data = {"email": "invalidemail"}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, invalid_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("email", response.data)

    def test_update_user_with_invalid_last_name(self):
        invalid_data = {"lastName": "a"*51}
        self.client.force_authenticate(user=self.user)

        with self.assertRaises(Exception) as err:
            response = self.client.put(self.url, invalid_data, format="json")

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("lastLame", response.data)

        self.assertEqual(str(err.exception), "value too long for type character varying(50)\n")


    def test_update_user_with_invalid_lang_key(self):
        invalid_data = {"langKey": "InvalidLanguageKey"}
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, invalid_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("langKey", response.data)
