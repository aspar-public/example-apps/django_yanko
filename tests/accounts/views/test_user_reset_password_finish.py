from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from backend.core.generators.user_key_generator import user_key_generator

UserModel = get_user_model()


class PasswordResetFinishViewTests(TestCase):
    """
        return: Test User Reset Password Finish View. Test reset finish with valid data and invalid data.
                Try with wrong 'reset_key', 'password'.
    """
    user_data = {
        "username": f"testuser{len(UserModel.objects.all())}",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+{len(UserModel.objects.all())}@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
        "is_active": True,
        "reset_key": user_key_generator(UserModel, "reset_key", 20)
    }

    def setUp(self):
        self.client = APIClient()
        self.user = UserModel.objects.create_user(
            **self.user_data,
        )

        self.url = reverse("reset password finish", kwargs={"key": self.user.reset_key})

    def test_password_reset_finish_view_with_valid_data(self):
        data = {
            "new_password": "Bla1Bla@Bla!",
            "key": self.user.reset_key,
        }
        get_response = self.client.get(self.url)
        self.assertEqual(get_response.data["message"], "Enter your new password")

        post_response = self.client.post(self.url, data, format="json")
        self.assertEqual(post_response.status_code, status.HTTP_200_OK)
        self.assertIn("message", post_response.data)

        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("Bla1Bla@Bla!"))
        self.assertIsNone(self.user.reset_key)

    def test_password_reset_finish_view_with_invalid_password(self):
        data = {
            "new_password": "ivalid",
        }
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST, f"{response.data}")
        self.assertIn("This password is too short. It must contain at least 8 characters.",
                      response.data["new_password"])

    def test_password_reset_finish_view_with_longer_invalid_key(self):
        invalid_key = "x" * 21
        url = reverse("reset password finish", kwargs={"key": invalid_key})
        data = {
            "new_password": "Bla1Bla@Bla!",
            "key": invalid_key,
        }
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("Ensure this field has no more than 20 characters.", response.data["key"][0])

    def test_password_reset_finish_view_with_invalid_key_with_proper_length(self):
        invalid_key = "A" * 20
        url = reverse("reset password finish", kwargs={"key": invalid_key})
        data = {
            "new_password": "Bla1Bla@Bla!",
            "key": invalid_key,
        }

        get_response = self.client.get(url)
        error_message = "Reset key is not valid. Check your link again or make request for new key."
        self.assertEqual(get_response.data["message"], error_message)

        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("Not valid key! Please, make new request for key!", response.data["non_field_errors"])
