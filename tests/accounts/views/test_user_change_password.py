from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

UserModel = get_user_model()


class ChangePasswordViewTests(TestCase):
    """
        return: Test User Change Password View. Test with valid input data and test with invalid data.
    """
    user_data = {
        "username": f"testuser{len(UserModel.objects.all())}",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+{len(UserModel.objects.all())}@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
        "is_active": True,
    }

    def setUp(self):
        self.client = APIClient()
        self.user = UserModel.objects.create_user(
            **self.user_data,
        )
        self.client.force_authenticate(user=self.user)

        self.url = reverse("change password")

    def test_account_owner_change_password_with_valid_data_successfully(self):
        data = {
            "old_password": self.user_data["password"],
            "password": "Bla1Bla@Bla!",
            "password2": "Bla1Bla@Bla!",
        }
        self.client.force_authenticate(user=self.user)
        response = self.client.put(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("Bla1Bla@Bla!"))

    def test_admin_user_change_password_with_valid_data_successfully(self):
        data = {
            "old_password": self.user_data["password"],
            "password": "Bla1Bla@Bla!",
            "password2": "Bla1Bla@Bla!",
        }
        admin_user = self.user
        admin_user.is_superuser = True
        admin_user.is_staff = True
        admin_user.save()

        self.client.force_authenticate(user=admin_user)
        response = self.client.put(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertTrue(self.user.check_password("Bla1Bla@Bla!"))

    def test_change_password_with_mismatched_first_passwords(self):
        data = {
            "old_password": self.user_data["password"],
            "password": "mismatchedpassword",
            "password2": "Bla1Bla@Bla!",
        }
        response = self.client.put(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("password", response.data)

    def test_change_password_with_mismatched_second_passwords(self):
        data = {
            "old_password": self.user_data["password"],
            "password": "Bla1Bla@Bla!",
            "password2": "mismatchedpassword",
        }
        response = self.client.put(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("password", response.data)

    def test_change_password_with_incorrect_old_password(self):
        data = {
            "old_password": "incorrectpassword",
            "password": "Bla1Bla@Bla!",
            "password2": "Bla1Bla@Bla!",
        }
        response = self.client.put(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("old_password", response.data)
