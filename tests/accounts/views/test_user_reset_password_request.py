from django.contrib.auth import get_user_model
from django.core import mail
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

UserModel = get_user_model()


class PasswordResetRequestViewTests(TestCase):
    """
        return: Test User Reset Password Request View. Test reset request with valid data and invalid data.
                Try with not valid email, and with not existing email.
    """
    user_data = {
        "username": f"testuser{len(UserModel.objects.all())}",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+{len(UserModel.objects.all())}@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
        "is_active": True,
    }

    def setUp(self):
        self.client = APIClient()
        self.user = UserModel.objects.create_user(
            **self.user_data
        )
        self.client.force_authenticate(user=self.user)
        self.url = reverse("reset password request")

    def test_password_reset_request_successfully(self):
        data = {
            "email": self.user_data["email"],
        }
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 2)

    def test_password_reset_request_with_nonexistent_email(self):
        data = {
            "email": "nonexistent@example.com",
        }
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        error_message = "Email does not exist. Please contact Administrator or check your email is correct."
        self.assertIn(error_message, response.data["non_field_errors"])

    def test_password_reset_request_with_invalid_email_format(self):
        data = {
            "email": "invalid-email",
        }
        response = self.client.post(self.url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("email", response.data)
