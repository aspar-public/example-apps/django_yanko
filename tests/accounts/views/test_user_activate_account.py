from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from backend.Accounts.serializers import JhiUserActivationAccountSerializer

UserModel = get_user_model()


class ActivateAccountJhiUserViewTests(TestCase):
    """
        return: Test Account Activation View with valid data and invalid data.
    """
    user_data = {
        "username": f"testuser00",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+01@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
    }
    user_data_activated = {
        "username": f"testuser01",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+02@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
        "is_active": True,
    }

    def setUp(self):
        self.user = UserModel.objects.create(
            **self.user_data,
        )
        self.user_activated = UserModel.objects.create(
            **self.user_data_activated,
        )

    def test_activation_view_success(self):
        key = self.user.activation_key
        url = reverse("account activation", kwargs={"key": key})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        self.user.refresh_from_db()
        self.assertTrue(self.user.is_active)

        expected_data = JhiUserActivationAccountSerializer(self.user).data
        self.assertEqual(response.data, expected_data)

    def test_activation_view_invalid_key(self):
        invalid_key = "invalidkey"
        url = reverse("account activation", kwargs={"key": invalid_key})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("error", response.data)

        self.user.refresh_from_db()
        self.assertFalse(self.user.is_active)

    def test_activation_view_already_activated_user(self):
        activated_user = self.user
        activated_user.is_active = True
        activated_user.save()

        key = activated_user.activation_key
        url = reverse("account activation", kwargs={"key": key})
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        expected_redirect_url = reverse("login")
        self.assertRedirects(response, expected_redirect_url, status_code=302, target_status_code=405)



