from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

UserModel = get_user_model()


class CreateJhiUserViewTest(APITestCase):
    """
        return: Test CreateJhiUserView for successfully created user with valid data and
                test to create user with invalid data.
    """
    user_data = {
        "login": "testuser00",
        "firstName": "Test",
        "lastName": "User",
        "email": f"no.reply.mymotomadness+{len(UserModel.objects.all())}@gmail.com",
        "imageUrl": "http://example.com/testuser.jpg",
        "langKey": "en",
        "password": "qwertY@12345",
    }

    def setUp(self):
        self.create_user_url = reverse("register")

    def test_create_user_with_valid_data(self):
        response = self.client.post(self.create_user_url, self.user_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        created_user = UserModel.objects.get(username=self.user_data["login"])
        self.assertIsNotNone(created_user)

        self.assertEqual(response.data["id"], created_user.id)
        self.assertEqual(response.data["login"], self.user_data["login"])

    def test_create_user_invalid_login(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["login"] = "X" * 51

        response = self.client.post(self.create_user_url, invalid_user_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Ensure this field has no more than 50 characters."
        self.assertEqual(response.data["login"][0], expected_message)

    def test_create_user_invalid_firstName(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["firstName"] = "X" * 51

        with self.assertRaises(Exception) as err:
            response = self.client.post(self.create_user_url, invalid_user_data, format="json")

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST,
                             f"Unexpected response: {response.content}")

        self.assertEqual(str(err.exception), "value too long for type character varying(50)\n")

    def test_create_user_invalid_lastName(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["lastName"] = "X" * 51

        with self.assertRaises(Exception) as err:
            response = self.client.post(self.create_user_url, invalid_user_data, format="json")

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(str(err.exception), "value too long for type character varying(50)\n")

    def test_create_user_invalid_email(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["email"] = "invalid@email"

        response = self.client.post(self.create_user_url, invalid_user_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["email"][0], "Enter a valid email address.")

    def test_create_user_invalid_password(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["password"] = "1234"

        response = self.client.post(self.create_user_url, invalid_user_data, format="json")

        error_message_00 = "This password is too short. It must contain at least 8 characters."
        error_message_01 = "This password is too common."
        error_message_02 = "This password is entirely numeric."
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["password"][0], error_message_00)
        self.assertEqual(response.data["password"][1], error_message_01)
        self.assertEqual(response.data["password"][2], error_message_02)

    def test_create_user_empty_login(self):
        invalid_user_data = self.user_data.copy()
        invalid_user_data["login"] = ""

        response = self.client.post(self.create_user_url, invalid_user_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["login"][0], "This field may not be blank.")

    def test_create_jhi_user_empty_fields(self):
        invalid_user_data = {
            "login": "",
            "firstName": "",
            "lastName": "",
            "email": "",
            "imageUrl": "",
            "langKey": "",
            "password": "",
        }

        response = self.client.post(self.create_user_url, data=invalid_user_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        expected_error = "This field may not be blank."
        self.assertEqual(response.data["login"][0], expected_error)
        self.assertEqual(response.data["email"][0], expected_error)
        self.assertEqual(response.data["langKey"][0], '"" is not a valid choice.')

    def test_create_with_existing_login(self):
        UserModel.objects.create_user(
            username=self.user_data["login"],
            first_name=self.user_data["firstName"],
            last_name=self.user_data["lastName"],
            email=self.user_data["email"] + "m",
            password=self.user_data["password"],
            image_url=self.user_data["imageUrl"],
            is_active=True,
        )

        response = self.client.post(self.create_user_url, self.user_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["login"][0], "User with that login exist.")

    def test_create_with_existing_email(self):
        UserModel.objects.create_user(
            username=self.user_data["login"] + "m",
            first_name=self.user_data["firstName"],
            last_name=self.user_data["lastName"],
            email=self.user_data["email"],
            password=self.user_data["password"],
            image_url=self.user_data["imageUrl"],
            is_active=True,
        )

        response = self.client.post(self.create_user_url, self.user_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["email"][0], "user with this email already exists.")
