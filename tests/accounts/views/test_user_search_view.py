from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

UserModel = get_user_model()


class SearchJhiUserViewTests(TestCase):
    """
        return: Test Search User View. Test all operands and fields with valid data and not valid data.
                Test that only admin user have permission to this view.
    """

    def setUp(self):
        self.client = APIClient()
        self.admin_user = UserModel.objects.create_superuser(
            username="admin",
            password="adminpassword",
            first_name="Admin",
            last_name="Adminov",
            email="admin@example.com",
            is_superuser=True,
            is_staff=True,
            is_active=True,

        )
        self.user1 = get_user_model().objects.create_user(
            username="testuser1",
            first_name="Johntest",
            last_name="Doeuser",
            password="testpassword1",
            email="test1@example.com",
            is_active=True,
        )
        self.user2 = get_user_model().objects.create_user(
            username="testuser2",
            first_name=None,
            last_name=None,
            password="testpassword2",
            email="test2@example.com",
            is_active=True,
        )

    def get_search_url(self, query_params):
        return reverse("search users") + f"?{query_params}"

    def test_search_jhi_user_with_valid_data(self):
        url = self.get_search_url("login.contains=testuser&firstName.contains=test&lastName.contains=user")

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["login"], "testuser1")

    def test_search_jhi_user_does_not_contain(self):
        url = self.get_search_url("login.doesNotContain=user")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["login"], "admin")

    def test_search_jhi_user_equals(self):
        url = self.get_search_url(f"firstName.equals={self.user1.first_name}")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["firstName"], self.user1.first_name)

    def test_search_jhi_user_not_equals(self):
        url = self.get_search_url(f"firstName.notEquals={self.user1.first_name}")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["firstName"], "Admin")

    def test_search_jhi_user_specified(self):
        url = self.get_search_url("lastName.specified=True")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

    def test_search_jhi_user_in(self):
        url = self.get_search_url("login.in=testuser1,testuser2,John,David,Pier&page_size=1")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 2)

    def test_search_jhi_user_not_in(self):
        url = self.get_search_url("login.notIn=John,David,Pier&page_size=1")
        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 3)
        self.assertEqual(response.data["results"][0]["login"], "admin")

    def test_search_jhi_user_with_invalid_field(self):
        url = self.get_search_url("invalidField.contains=testuser")

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("'invalidField' is not valid field", response.data["message"])

    def test_search_jhi_user_with_invalid_operator(self):
        url = self.get_search_url("login.invalidOperator=testuser&page_size=1")

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        error_message = "Entered 'invalidOperator' is not valid operator. Allowed operands: " \
                        "`contains`, `doesNotContain`, `equals`, `notEquals`, `specified`, `in`, `notIn`!"

        self.assertIn(error_message, response.data["message"])

    def test_search_jhi_user_with_invalid_value_and_specified_operator(self):
        url = self.get_search_url("login.specified=Falsee&page_size=1")

        self.client.force_authenticate(user=self.admin_user)
        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        error_message = "Options for operator 'specified' must be 'True' or 'False'. You have entered 'Falsee'."
        self.assertIn(error_message, response.data["message"])

    def test_search_jhi_user_unauthenticated(self):
        url = self.get_search_url("login.contains=testuser1")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
