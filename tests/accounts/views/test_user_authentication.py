from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

UserModel = get_user_model()


class LoginJhiUserViewTests(TestCase):
    """
        return: Test User Login View to check JWT life and not unsuccessful try with invalid credentials.
    """
    user_data = {
        "username": "testuser00",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+{len(UserModel.objects.all())}@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
        "is_active": True
    }

    def setUp(self):
        self.client = APIClient()
        self.user = UserModel.objects.create_user(
            **self.user_data,
        )

    def test_login_with_remember_me(self):
        url = reverse("login")
        data = {
            "username": self.user_data["username"],
            "password": self.user_data["password"],
            "rememberMe": True,
        }
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id_token", response.data)
        token = response.data["id_token"]

        decoded_token = JSONWebTokenAuthentication.jwt_decode_token(token)
        expiration_delta = decoded_token["exp"]
        self.assertGreaterEqual(expiration_delta, 24 * 60 * 60)

    def test_login_without_remember_me(self):
        url = reverse("login")
        data = {
            "username": self.user_data["username"],
            "password": self.user_data["password"],
        }
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id_token", response.data)
        token = response.data["id_token"]

        decoded_token = JSONWebTokenAuthentication.jwt_decode_token(token)
        expiration_delta = decoded_token["exp"]
        self.assertGreaterEqual(expiration_delta, 300)

    def test_login_with_invalid_password(self):
        url = reverse("login")
        data = {
            "username": self.user_data["username"],
            "password": "invalidpassword",
        }
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn("token", response.data)

    def test_login_with_invalid_username(self):
        url = reverse("login")
        data = {
            "username": "invaliduser",
            "password": self.user_data["password"],
        }
        response = self.client.post(url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertNotIn("token", response.data)
