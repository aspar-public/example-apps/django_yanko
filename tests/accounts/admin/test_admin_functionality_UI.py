# from django.contrib.auth import get_user_model
# from django.test import LiveServerTestCase
# from selenium.webdriver.chrome.webdriver import WebDriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.ui import WebDriverWait
#
# UserModel = get_user_model()
#
#
# class AdminJhiUITestCase(LiveServerTestCase):
#     """
#         return: Test Admin User functionalities with UI tests.
#     """
#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         cls.browser = WebDriver()
#
#     @classmethod
#     def tearDownClass(cls):
#         cls.browser.quit()
#         super().tearDownClass()
#
#     def setUp(self):
#         self.user = UserModel.objects.create(
#             username="testuser",
#             email="test@example.com",
#             password="qwertY@12345"
#         )
#         self.admin_user = UserModel.objects.create_superuser(
#             username="admin",
#             password="qwertY@12345",
#             email="admin@example.com",
#         )
#         self.updated_password = "S1r0ngP@assword"
#         self.first_name = "NewFirstName"
#         self.last_name = "NewLastName"
#
#     def test_save_model_password_update(self):
#         self.browser.get(self.live_server_url + "/admin/")
#
#         self.browser.find_element(By.ID, "id_username").send_keys("admin")
#         self.browser.find_element(By.ID, "id_password").send_keys("qwertY@12345")
#         self.browser.find_element(By.CSS_SELECTOR, "input[type='submit']").click()
#         self.browser.find_element(By.LINK_TEXT, "Users").click()
#         self.browser.find_element(By.LINK_TEXT, self.user.username).click()
#
#         wait = WebDriverWait(self.browser, 30)
#         wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#id_password")))
#
#         password_field = self.browser.find_element(By.CSS_SELECTOR, "#id_password")
#         password_field.clear()
#         password_field.send_keys(self.updated_password)
#
#         self.browser.find_element(By.CSS_SELECTOR, "input[name='_save']").click()
#
#         self.user.refresh_from_db()
#         self.assertTrue(self.user.check_password(self.updated_password))
#
#     def test_save_model_first_name_update(self):
#         self.browser.get(self.live_server_url + "/admin/")
#
#         self.browser.find_element(By.ID, "id_username").send_keys("admin")
#         self.browser.find_element(By.ID, "id_password").send_keys("qwertY@12345")
#         self.browser.find_element(By.CSS_SELECTOR, "input[type='submit']").click()
#         self.browser.find_element(By.LINK_TEXT, "Users").click()
#         self.browser.find_element(By.LINK_TEXT, self.user.username).click()
#
#         wait = WebDriverWait(self.browser, 30)
#         wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#id_first_name")))
#
#         first_name_field = self.browser.find_element(By.CSS_SELECTOR, "#id_first_name")
#         first_name_field.clear()
#         first_name_field.send_keys(self.first_name)
#
#         self.browser.find_element(By.CSS_SELECTOR, "input[name='_save']").click()
#
#         self.user.refresh_from_db()
#         self.assertEqual(self.first_name, self.user.first_name)
#
#     def test_save_model_last_name_update(self):
#         self.browser.get(self.live_server_url + "/admin/")
#
#         self.browser.find_element(By.ID, "id_username").send_keys("admin")
#         self.browser.find_element(By.ID, "id_password").send_keys("qwertY@12345")
#         self.browser.find_element(By.CSS_SELECTOR, "input[type='submit']").click()
#         self.browser.find_element(By.LINK_TEXT, "Users").click()
#         self.browser.find_element(By.LINK_TEXT, self.user.username).click()
#
#         wait = WebDriverWait(self.browser, 30)
#         wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#id_last_name")))
#
#         last_name_field = self.browser.find_element(By.CSS_SELECTOR, "#id_last_name")
#         last_name_field.clear()
#         last_name_field.send_keys(self.last_name)
#
#         self.browser.find_element(By.CSS_SELECTOR, "input[name='_save']").click()
#
#         self.user.refresh_from_db()
#         self.assertEqual(self.last_name, self.user.last_name)
