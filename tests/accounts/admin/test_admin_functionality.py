from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.admin.sites import AdminSite
from django.test import RequestFactory
from backend.Accounts.admin import AdminJhi

UserModel = get_user_model()


class AdminJhiTests(TestCase):
    """
        return: Test Admin User functionalities.
    """

    def setUp(self):
        self.client = Client()
        self.admin_user = UserModel.objects.create_superuser(
            username="admin",
            password="qwertY@12345",
            email="admin@example.com",
        )
        self.client.force_login(self.admin_user)
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            email="test@example.com",
            first_name="Test",
            last_name="User",
            is_active=True,
        )

        self.admin_site = AdminSite()
        self.request_factory = RequestFactory()
    def test_admin_list_view(self):
        url = reverse("admin:Accounts_jhiuser_changelist")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.username)

    def test_admin_change_view(self):
        url = reverse("admin:Accounts_jhiuser_change", args=[self.user.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Change user")

    def test_admin_add_view(self):
        url = reverse("admin:Accounts_jhiuser_add")
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Add user")

    def test_admin_delete_view(self):
        url = reverse("admin:Accounts_jhiuser_delete", args=[self.user.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Are you sure you want to delete")

    def test_admin_search(self):
        url = reverse("admin:Accounts_jhiuser_changelist")
        data = {"q": "testuser"}
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.username)

    def test_admin_filter(self):
        url = reverse("admin:Accounts_jhiuser_changelist")
        data = {"is_active": "1"}
        response = self.client.get(url, data)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, self.user.username)

    def test_save_model_password_update(self):
        request = self.request_factory.post("/admin/")
        request.user = self.user

        admin_instance = AdminJhi(UserModel, self.admin_site)

        updated_password = "S1r0ngP@assword"
        form_data = {
            "password": updated_password,
        }

        self.user.set_password(updated_password)
        form = admin_instance.get_form(request)(form_data, instance=self.user)
        admin_instance.save_model(request, self.user, form, True)

        self.assertTrue(self.user.check_password(updated_password))

    def test_save_model_no_password_update(self):
        request = self.request_factory.post("/admin/")
        request.user = self.user

        admin_instance = AdminJhi(UserModel, self.admin_site)

        form_data = {
            "password": "",
        }
        form = admin_instance.get_form(request)(form_data, instance=self.user)
        admin_instance.save_model(request, self.user, form, True)


        self.assertTrue(self.user.check_password("qwertY@12345"))