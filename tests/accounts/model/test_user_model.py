from datetime import datetime
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.db.utils import DataError
from django.test import TestCase

UserModel = get_user_model()

class JhiUserModelTests(TestCase):
    """
        return: Tests for User Model.
    """

    def setUp(self):
        self.user_data = {
            "username": "testuser",
            "first_name": None,
            "last_name": None,
            "password": "qwertY@12345",
            "email": "test@example.com",
            "lang_key": "en",
        }

    def test_create_user(self):
        user = UserModel.objects.create_user(**self.user_data)

        self.assertEqual(user.username, self.user_data["username"])
        self.assertTrue(user.check_password(self.user_data["password"]))
        self.assertEqual(user.email, self.user_data["email"])
        self.assertFalse(user.is_active)
        self.assertIsNotNone(user.activation_key)
        self.assertIsNone(user.reset_key)
        self.assertIsNone(user.reset_day)
        self.assertIsNotNone(user.lang_key)
        self.assertIsNone(user.first_name)
        self.assertIsNone(user.last_name)
        self.assertIsNone(user.image_url)

    def test_save_user_password_encryption(self):
        user = UserModel(**self.user_data)
        user.save()

        self.assertTrue(user.check_password(self.user_data["password"]))
        self.assertTrue("bcrypt" in user.password)

    def test_save_user_activation_key_generation(self):
        user = UserModel(**self.user_data)
        user.save()

        self.assertIsNotNone(user.activation_key)
        self.assertEqual(len(user.activation_key), 20)

    def test_save_superuser_activation(self):
        superuser_data = {
            "username": "admin",
            "password": "qwertY@12345",
            "email": "admin@example.com",
            "is_superuser": True,
        }

        superuser = UserModel(**superuser_data)
        superuser.save()

        self.assertTrue(superuser.is_active)

    def test_save_user_last_modified_fields(self):
        user = UserModel(**self.user_data)
        user.save()

        self.assertIsNotNone(user.last_modified_date)
        self.assertEqual(user.last_modified_by, "anonymousUser")

    def test_invalid_email(self):
        invalid_email_data = {
            "username": "testuser",
            "password": "qwertY@12345",
            "email": "invalid",
        }

        try:
            user = get_user_model()(**invalid_email_data)
            user.clean_fields()
            user.save()
        except ValidationError as e:
            self.assertIn("Enter a valid email address.", e.message_dict.get('email', ''))
        else:
            self.fail(f"ValidationError not raised. User: {user}")

    def test_valid_lang_key(self):
        user_data_with_lang_key = {
            "username": "testuser",
            "password": "qwertY@12345",
            "email": "test@example.com",
            "lang_key": "en",
        }

        user = UserModel(**user_data_with_lang_key)
        user.save()

        self.assertEqual(user.lang_key, "en")

    def test_invalid_lang_key(self):
        user_data_with_invalid_lang_key = {
            "username": "testuser",
            "password": "qwertY@12345",
            "email": "test@example.com",
            "lang_key": "invalidlang",
        }

        with self.assertRaises(DataError) as err:
            get_user_model().objects.create(**user_data_with_invalid_lang_key)

        error_message = "value too long for type character varying(10)\n"
        self.assertEqual(str(err.exception), error_message)
