from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase

from backend.Accounts.helpers import find_roles_of_user
from backend.Accounts.serializers import JhiUserCreateSerializer

UserModel = get_user_model()


class JhiUserCreateSerializerTest(APITestCase):
    """
        return: Test class for the User Register Serializer.
    """

    def setUp(self):
        self.valid_user_data = {
            "login": "testuser22",
            "firstName": "Test",
            "lastName": "User",
            "email": "test@example.com",
            "imageUrl": "http://example.com/testuser.jpg",
            "langKey": "en",
            "password": "qwertY@12345",
        }

        self.invalid_user_data = {
            "login": "testuser23",
            "firstName": "Test",
            "lastName": "User",
            "email": "invalidemail",
            "imageUrl": "http://example.com/testuser.jpg",
            "langKey": "en",
            "password": "inv",
        }

    def test_valid_user_data_serializer(self):
        serializer = JhiUserCreateSerializer(data=self.valid_user_data)

        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.errors, {})

    def test_invalid_user_data_serializer(self):
        serializer = JhiUserCreateSerializer(data=self.invalid_user_data)
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors["email"][0], "Enter a valid email address.")
        self.assertEqual(serializer.errors["password"][0],
                         "This password is too short. It must contain at least 8 characters.")

    def test_to_representation(self):
        user = UserModel.objects.create_user(
            username=self.valid_user_data["login"],
            first_name=self.valid_user_data["firstName"],
            last_name=self.valid_user_data["lastName"],
            email=self.valid_user_data["email"],
            image_url=self.valid_user_data["imageUrl"],
            lang_key=self.valid_user_data["langKey"],
            password=self.valid_user_data["password"],
        )

        serializer = JhiUserCreateSerializer(instance=user)
        expected_data = {
            "id": user.id,
            "login": user.username,
            "firstName": user.first_name,
            "lastName": user.last_name,
            "email": user.email,
            "imageUrl": user.image_url,
            "langKey": user.lang_key,
            "activated": user.is_active,
            "createdBy": user.created_by,
            "createdDate": user.date_joined,
            "lastModifiedBy": user.last_modified_by,
            "lastModifiedDate": user.last_modified_date,
            "authorities": find_roles_of_user(user),
        }
        self.assertEqual(serializer.data, expected_data)
