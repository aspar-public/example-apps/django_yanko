from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase, APIClient
from rest_framework import status

from backend.Accounts.serializers import JhiUserChangePasswordSerializer

UserModel = get_user_model()


class JhiUserChangePasswordSerializerTest(APITestCase):
    """
        return: Tests for Change Password Serializer.
    """
    def setUp(self):
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            email="test@email.com",
            is_active=True,
        )
        self.client = APIClient()
        self.client.force_login(user=self.user)
        self.valid_password_data = {
            "old_password": "qwertY@12345",
            "password": "NewPassword123",
            "password2": "NewPassword123",
        }
        self.invalid_password_data = {
            "old_password": "invalidoldpassword",
            "password": "InvalidPassword",
            "password2": "MismatchPassword",
        }

    def test_valid_password_change_serializer(self):
        url = f"/change-password/{self.user.pk}/"
        response = self.client.post(url)
        request = response.wsgi_request
        serializer = JhiUserChangePasswordSerializer(
            instance=self.user, data=self.valid_password_data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        self.assertEqual(serializer.errors, {})

    def test_invalid_old_password_serializer(self):
        url = f"/change-password/{self.user.pk}/"
        invalid_data = self.valid_password_data.copy()
        invalid_data["old_password"] = "invalidoldpassword"
        response = self.client.post(url)
        request = response.wsgi_request
        serializer = JhiUserChangePasswordSerializer(
            instance=self.user, data=invalid_data, context={"request": request}
        )

        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors["old_password"]["old_password"], "Old password is not correct")

    def test_password_mismatch_serializer(self):
        url = f"/change-password/{self.user.pk}/"
        invalid_data = self.valid_password_data.copy()
        invalid_data["password2"] = "MismatchedPassword"
        response = self.client.post(url)
        request = response.wsgi_request
        serializer = JhiUserChangePasswordSerializer(
            instance=self.user, data=invalid_data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(serializer.errors["password"][0], "Password fields didn't match.")

    def test_invalid_password_serializer(self):
        url = f"/change-password/{self.user.pk}/"
        invalid_data = self.valid_password_data.copy()
        invalid_data["password"] = "weak"
        response = self.client.post(url)
        request = response.wsgi_request
        serializer = JhiUserChangePasswordSerializer(
            instance=self.user, data=invalid_data, context={"request": request}
        )
        self.assertFalse(serializer.is_valid())
        self.assertEqual(
            serializer.errors["password"][0],
            "This password is too short. It must contain at least 8 characters.",
        )

    def test_update_method(self):
        url = f"/change-password/{self.user.pk}/"
        response = self.client.post(url)
        request = response.wsgi_request
        serializer = JhiUserChangePasswordSerializer(
            instance=self.user, data=self.valid_password_data, context={"request": request}
        )
        self.assertTrue(serializer.is_valid())
        updated_user = serializer.update(self.user, serializer.validated_data)
        self.assertTrue(updated_user.check_password(self.valid_password_data["password"]))
        self.assertEqual(updated_user, self.user)
