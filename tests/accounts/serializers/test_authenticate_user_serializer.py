from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APIRequestFactory
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings

from backend.Accounts.serializers import JhiUserLoginSerializer

UserModel = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class JhiUserLoginSerializerTest(TestCase):
    """
        return: Test Login Serializer with test cases with  valid and invalid credentials.

    """
    def setUp(self):
        self.user_data = {
            "username": "testuser",
            "password": "qwertY@12345",
            "is_active": True,
        }

        self.user = UserModel.objects.create_user(**self.user_data)
        self.client = APIClient()
        self.url = reverse("login")

    def test_authenticate_with_valid_credentials_and_rememberMe_false(self):
        login_data = {
            "username": "testuser",
            "password": "qwertY@12345",
            "rememberMe": False,
        }

        response = self.client.post(self.url, data=login_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn("id_token", response.data)

        self.assertEqual(response.data["rememberMe"], "False")
        self.assertEqual(login_data["username"], response.data.get("username"))

    def test_authenticate_with_valid_credentials_and_rememberMe_true(self):
        data = {
            'username': 'testuser',
            'password': 'qwertY@12345',
            'rememberMe': True
        }

        factory = APIRequestFactory()
        request = factory.post(self.url, data, format='json')
        serializer = JhiUserLoginSerializer(data=data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        result = serializer.validated_data

        self.assertIn('token', result)
        self.assertTrue('exp' in JSONWebTokenAuthentication.jwt_decode_token(result['token']))
        self.assertTrue(result["remember_me"])
    def test_invalid_login_credentials(self):
        invalid_login_data = {
            "username": "testuser",
            "password": "wrongpassword",
            "rememberMe": False,
        }

        response = self.client.post(self.url, data=invalid_login_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("non_field_errors", response.data)

    def test_missing_password(self):
        missing_credentials_data = {
            "username": "testuser",
            "rememberMe": False,
        }

        response = self.client.post(self.url, data=missing_credentials_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("password", response.data)

