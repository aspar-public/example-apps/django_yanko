from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework import serializers
from rest_framework.test import APIRequestFactory

from backend.Accounts.serializers import JhiUserResetPasswordFinishSerializer

UserModel = get_user_model()


class JhiUserResetPasswordFinishSerializerTest(TestCase):
    """
        return: Test Class for Reset Password Finish Serializer.
    """
    def setUp(self):
        self.user_data = {
            "username": "testuser",
            "password": "qwertY@12345",
            "is_active": True,
            "reset_key": "Afw3rFvJHIOKAe2459kl"
        }

        self.user = UserModel.objects.create_user(**self.user_data)
        self.factory = APIRequestFactory()

    def test_invalid_reset_key(self):
        serializer_data = {
            "new_password": "new_password",
            "key": "invalidAkeyA123awerf",
        }

        serializer = JhiUserResetPasswordFinishSerializer(data=serializer_data, context={"reset_key": "valid_key"})
        with self.assertRaises(serializers.ValidationError) as err:
            serializer.is_valid(raise_exception=True)

        self.assertEqual(serializer.errors["non_field_errors"][0], "Not valid key! Please, make new request for key!")

    def test_copy_key_from_email(self):
        serializer_data = {
            "new_password": "qwertY@123451",
            "key": "Afw3rFvJHIOKAe2459ka",
        }

        serializer = JhiUserResetPasswordFinishSerializer(data=serializer_data, context="Afw3rFvJHIOKAe2459kl")
        with self.assertRaises(serializers.ValidationError) as err:
            serializer.is_valid(raise_exception=True)

        error_message = err.exception.get_full_details()["non_field_errors"][0]["message"]

        self.assertEqual(error_message, "Please copy again key from the email!")
