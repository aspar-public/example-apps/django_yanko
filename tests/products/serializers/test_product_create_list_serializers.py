from django.contrib.auth import get_user_model
from django.test import TestCase

from backend.Products.models import ProductModel
from backend.Products.serializers import ListCreateProductSerializer

UserModel = get_user_model()


class ListCreateProductSerializerTest(TestCase):
    """
        return: Test Class for List Create Product Serializer.
    """

    def setUp(self):
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            is_active=True,
            is_staff=True,
            is_superuser=True,
        )

        self.product_data_owner = {
            "code": "ABC123",
            "name": "Test Product",
            "description": "A test product",
            "price": 9.99,
            "owner": self.user,
        }
        self.product_data_no_owner = {
            "code": "ABC123",
            "name": "Test Product",
            "description": "A test product",
            "price": 9.99,

        }

    def test_serializer_with_valid_data(self):
        serializer = ListCreateProductSerializer(data=self.product_data_no_owner)
        serializer.is_valid()
        product = serializer.save()

        self.assertEqual(product.code, "ABC123")
        self.assertEqual(product.name, "Test Product")
        self.assertEqual(product.description, "A test product")
        self.assertEqual(float(9.99), 9.99)

    def test_serializer_with_duplicate_code(self):
        existing_product = ProductModel.objects.create(**self.product_data_owner)

        serializer = ListCreateProductSerializer(data=self.product_data_owner)
        self.assertFalse(serializer.is_valid())
        self.assertIn("code", serializer.errors)

    def test_serializer_with_blank_name(self):
        invalid_data = {
            "code": "XYZ789",
            "name": "",
        }
        serializer = ListCreateProductSerializer(data=invalid_data)
        self.assertFalse(serializer.is_valid())
        self.assertIn("name", serializer.errors)

    def test_serializer_with_valid_null_fields(self):
        null_fields_data = {
            "code": "XYZ789",
            "name": "Product with Null Fields",
            "owner": self.user.id,
        }
        serializer = ListCreateProductSerializer(data=null_fields_data)
        self.assertTrue(serializer.is_valid())
        product = serializer.save()

        self.assertEqual(product.description, None)
        self.assertEqual(product.price, None)

    def test_serializer_representation(self):
        product = ProductModel.objects.create(**self.product_data_owner)
        serializer = ListCreateProductSerializer(instance=product)

        expected_representation = {
            "id": product.id,
            "code": "ABC123",
            "name": "Test Product",
            "description": "A test product",
            "price": 9.99,
            "image01": [product.image_01],
            "image01ContentType": product.image_01_content_type,
            "owner": {
                "id": self.user.id,
                "login": self.user.username,
            },
        }

        self.assertEqual(serializer.data, expected_representation)
