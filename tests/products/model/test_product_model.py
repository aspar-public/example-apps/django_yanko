from django.contrib.auth import get_user_model
from django.test import TestCase

from backend.Products.models import ProductModel

UserModel = get_user_model()


class ProductModelTest(TestCase):
    """
        return: Test Class for Product Model.
    """

    def setUp(self):
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            is_active=True,
        )
        self.product_data = {
            "code": "ABC123",
            "name": "Test Product",
            "description": "A test product",
            "price": 9.99,
            "owner": self.user,
        }

    def test_create_product(self):
        product = ProductModel.objects.create(**self.product_data)
        self.assertEqual(product.code, "ABC123")
        self.assertEqual(product.name, "Test Product")
        self.assertEqual(product.description, "A test product")
        self.assertEqual(product.price, 9.99)
        self.assertEqual(product.owner, self.user)

        expected_message = f"id: {product.id}, code: {product.code}, name: {product.name}."
        self.assertEqual(str(product), expected_message)

    def test_unique_code(self):
        ProductModel.objects.create(**self.product_data)

        duplicate_product_data = {
            "code": "ABC123",
            "name": "Duplicate Product",
            "description": "A duplicate product",
            "price": 19.99,
            "owner": self.user,
        }
        with self.assertRaises(Exception):
            ProductModel.objects.create(**duplicate_product_data)

    def test_null_fields(self):

        null_fields_data = {
            "code": "XYZ789",
            "name": "Product with Null Fields",
            "owner": self.user,
        }
        product = ProductModel.objects.create(**null_fields_data)
        self.assertEqual(product.description, None)
        self.assertEqual(product.price, None)

    def test_relationship_with_owner(self):
        product = ProductModel.objects.create(**self.product_data)
        self.assertEqual(product.owner, self.user)

        self.assertIn(product, self.user.productmodel_set.all())