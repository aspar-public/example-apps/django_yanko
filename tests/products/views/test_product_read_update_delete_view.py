from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from backend.Products.models import ProductModel

UserModel = get_user_model()


class TestProductsDataActionsView(APITestCase):
    """
        return: Test ProductsDataActionsView for read, update and delete product.
    """
    def setUp(self):
        self.user = UserModel.objects.create_user(
            username='testuser',
            password='qwertY@12345',
            is_active=True
        )
        self.client.force_authenticate(user=self.user)
        self.product = ProductModel.objects.create(
            code='123',
            name='Test Product',
            description='This is a test product',
            price='10.99',
            owner=self.user
        )
        self.url = reverse('product data actions', kwargs={'id': self.product.id})

    def test_retrieve_product(self):
        response = self.client.get(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['code'], self.product.code)
        self.assertEqual(response.data['name'], self.product.name)


    def test_retrieve_nonexistent_product(self):
        nonexistent_url = reverse('product data actions', kwargs={'id': 999})
        response = self.client.get(nonexistent_url, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual("Product not exist.", response.data[0])



    def test_update_product(self):
        data = {
            'code': '456',
            'name': 'Updated Product',
            'description': 'This product has been updated',
            'price': '15.99',
        }

        response = self.client.put(self.url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        updated_product = ProductModel.objects.get(id=self.product.id)
        self.assertEqual(updated_product.code, data['code'])
        self.assertEqual(updated_product.name, data['name'])
        # Add more assertions based on your serializer and expected response

    def test_delete_product(self):
        response = self.client.delete(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(Exception) as err:
            ProductModel.objects.get(id=self.product.id)

        self.assertEqual("ProductModel matching query does not exist.", str(err.exception))

    def test_update_nonexistent_product(self):
        nonexistent_url = reverse('product data actions', kwargs={'id': 999})
        data = {
            'code': '456',
            'name': 'Updated Product',
            'description': 'This product has been updated',
            'price': '15.99',
        }

        response = self.client.put(nonexistent_url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual("Product not exist.", response.data[0])

