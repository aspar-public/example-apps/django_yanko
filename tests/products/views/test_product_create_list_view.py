from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from backend.Products.models import ProductModel

UserModel = get_user_model()


class TestListCreateProductView(APITestCase):
    """
        return: Test ListCreateProductView for successfully created product with valid data and invalid data.
    """

    def setUp(self):
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            is_active=True
        )
        self.client.force_authenticate(user=self.user)
        self.url = reverse("list create product")
        for i in range(20):
            ProductModel.objects.create(
                code=f"00{i}",
                name=f"Search Product {i}",
                owner=self.user,
                image_01_content_type="jpeg"
            )

    def create_product(self, data):
        return self.client.post(self.url, data, format="json")

    def test_create_product(self):
        data = {
            "code": "testcode",
            "name": "Test Product",
            "description": "This is a test product",
            "price": "10.99",
            "image_01": "base64encodedimage",
            "image_01_content_type": "image/jpeg",
            "owner": self.user.id,
        }

        response = self.create_product(data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["code"], data["code"])
        self.assertEqual(response.data["name"], data["name"])

    def test_create_product_invalid_data(self):
        data = {
            "code": "",
            "name": "Test Product",
            "description": "This is a test product",
            "price": "10.99",
        }

        response = self.create_product(data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_search_product_valid_data(self):
        response = self.client.get(self.url + "?name.contains=Search Product", format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 20)

        response = self.client.get(self.url + "?name.doesNotContain=Search Product", format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_search_product_invalid_schema(self):
        response = self.client.get(self.url + "?names=Search Product", format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Invalid searching schema: {'field.operator': 'names', 'value': 'Search Product'}. " \
                           "Valid searching schemas: [/?field.operator=value], " \
                           "[/?field.operator=value&field2.operator2=value2] !"
        self.assertEqual(expected_message, response.data["message"])

    def test_search_product_valid_operator_for_not_proper_valid_field(self):
        response = self.client.get(self.url + "?description.lessThan=5", format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Attention: `greaterThan`, `greaterThanOrEqual`, `lessThan`, `lessThanOrEqual` is " \
                           "valid operators only for fields: `id`, `price`!"
        self.assertEqual(expected_message, response.data["message"])

    def test_search_product_invalid_field(self):
        response = self.client.get(self.url + "?image01Type.contains=jpeg", format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Entered 'image01Type' is not valid field. Allowed fields: `id`, `code`, `name`, " \
                           "`description`, `image01ContentType`, `price`, `owner`."
        self.assertEqual(expected_message, response.data["message"])

    def test_search_product_invalid_operand(self):
        response = self.client.get(self.url + "?name.doesNotCont=Search Product", format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Entered 'doesNotCont' is not valid operator. Allowed operands: `contains`, " \
                           "`doesNotContain`, `equals`, `notEquals`, `specified`, `in`, `notIn`, `greaterThan`, " \
                           "`greaterThanOrEqual`, `lessThan`, `lessThanOrEqual`!"
        self.assertEqual(expected_message, response.data["message"])

    def test_pagination(self):
        response = self.client.get(self.url + "?page_size=2", format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 5)
        self.assertIn("next", response.data)
        self.assertIn("previous", response.data)
