from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from backend.Products.models import ProductModel

UserModel = get_user_model()


class TestProductsCountView(APITestCase):
    """
        return: Test ProductsCountView for successfully created product with valid data and invalid data.
    """

    def setUp(self):
        self.user = UserModel.objects.create_user(
            username="testuser",
            password="qwertY@12345",
            is_active=True
        )
        self.client.force_authenticate(user=self.user)
        self.url = reverse("products count")
        for i in range(5):
            ProductModel.objects.create(code=f"code{i}", name=f"Test Product{i}", owner=self.user)

    def test_count_products(self):
        response = self.client.get(self.url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("count", response.data)
        self.assertEqual(response.data["count"], 5)

    def test_count_products_with_search_with_valid_data(self):
        response = self.client.get(self.url + "?name.contains=Test", format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("count", response.data)
        self.assertEqual(response.data["count"], 5)

    def test_count_products_with_search_with_invalid_operator(self):
        response = self.client.get(self.url + "?name.contain=Test", format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Entered 'contain' is not valid operator. Allowed operands: `contains`, " \
                           "`doesNotContain`, `equals`, `notEquals`, `specified`, `in`, `notIn`, `greaterThan`, " \
                           "`greaterThanOrEqual`, `lessThan`, `lessThanOrEqual`!"
        self.assertEqual(expected_message, response.data["message"])

    def test_count_products_with_search_with_invalid_field(self):
        response = self.client.get(self.url + "?nam.contains=Test", format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        expected_message = "Entered 'nam' is not valid field. Allowed fields: `id`, `code`, `name`, "\
                           "`description`, `image01ContentType`, `price`, `owner`."
        self.assertEqual(expected_message, response.data["message"])

