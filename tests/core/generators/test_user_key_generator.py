from django.test import TestCase

from backend.Accounts.models import JhiUser
from backend.core.generators.user_key_generator import user_key_generator


class UserKeyGeneratorTest(TestCase):
    """
        return: Test class that tests User Key Generator.
    """
    user_data = {
        "username": f"testuser00",
        "first_name": "Test",
        "last_name": "User",
        "email": f"no.reply.mymotomadness+01@gmail.com",
        "image_url": "http://example.com/testuser.jpg",
        "lang_key": "en",
        "password": "qwertY@12345",
    }

    def setUp(self):
        self.user = JhiUser.objects.create(
            **self.user_data,
        )

    def test_user_key_generator_unique_key(self):
        test_instance = self.user

        generated_key = user_key_generator(JhiUser, "reset_key", 10)

        setattr(test_instance, "reset_key", generated_key)
        test_instance.save()

        new_generated_key = user_key_generator(JhiUser, "reset_key", 10)
        self.assertNotEqual(generated_key, new_generated_key)

    def test_user_key_generator_existing_key(self):
        test_instance = self.user

        existing_key = "existing_key"
        setattr(test_instance, "reset_key", existing_key)
        test_instance.save()

        new_generated_key = user_key_generator(JhiUser, "reset_key", 10)
        self.assertNotEqual(existing_key, new_generated_key)
