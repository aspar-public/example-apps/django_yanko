
from rest_framework import exceptions
from unittest import TestCase
from backend.core.validators.serializer_validators import value_is_not_numeric_and_alphabetic


class SerializerValidatorsTests(TestCase):
    """
         return: Test class for testing custom serializer validators.
     """

    def test_valid_alphanumeric_value(self):

        try:
            value_is_not_numeric_and_alphabetic("Valid123")
        except exceptions.ValidationError:
            self.fail("Unexpected ValidationError for a valid alphanumeric value")

    def test_valid_alphabetic_value(self):

        try:
            value_is_not_numeric_and_alphabetic("ValidLetters")
        except exceptions.ValidationError:
            self.fail("Unexpected ValidationError for a valid alphabetic value")

    def test_valid_numeric_value(self):

        try:
            value_is_not_numeric_and_alphabetic("123456")
        except exceptions.ValidationError:
            self.fail("Unexpected ValidationError for a valid numeric value")

    def test_invalid_special_characters_value(self):

        with self.assertRaises(exceptions.ValidationError):
            value_is_not_numeric_and_alphabetic("Invalid!@#")

    def test_empty_value(self):

        with self.assertRaises(exceptions.ValidationError):
            value_is_not_numeric_and_alphabetic("")
