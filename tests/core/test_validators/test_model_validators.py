from django.test import TestCase
from django.core.exceptions import ValidationError
from backend.core.validators.model_validators import long_password_validator


class LongPasswordValidatorTest(TestCase):
    """
        return: Test class for testing custom password validator for long password.
    """

    def test_valid_password(self):
        valid_password = "a" * 67
        result = long_password_validator(valid_password)

        self.assertEqual(result, None)

    def test_invalid_long_password(self):
        invalid_password = "a" * 68
        with self.assertRaises(ValidationError) as err:
            long_password_validator(invalid_password)

        self.assertEqual(
            str(*err.exception),
            f"This password is too Long. It must contain maximum 67 characters, your contains {len(invalid_password)}."
        )

    def test_with_none_value(self):
        value = ""
        result = long_password_validator(value)

        self.assertEqual(result, None)

