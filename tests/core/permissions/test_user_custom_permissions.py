from datetime import datetime
from unittest.mock import patch

import jwt
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.models import AnonymousUser
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory, force_authenticate, APIClient

from backend import settings
from backend.core.permissions.permissions import IsAdminOrOwner
from backend.Accounts.views import JhiUserDataActionsView

UserModel = get_user_model()


class IsAdminOrOwnerTestCase(TestCase):

    """
        return: Test Class that test User permissions.
    """

    user_data = {
        "username": "testuser",
        "password": "qwertY@12345",
        "email": "test.email.com",
        "is_active": True,
    }

    def setUp(self):
        self.user = UserModel.objects.create_user(
            **self.user_data
        )
        self.test_object = UserModel.objects.create(
            username="testobject",
            password="qwertY@12345",
            email="test2email.com"
        )
        self.view = JhiUserDataActionsView.as_view()
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.login_url = reverse("login")

    def test_has_permission_authenticated_user_with_token(self):
        data = {
            "username": self.user_data["username"],
            "password": self.user_data["password"],
        }
        request = self.factory.get("/api/accounts/")
        authenticate(request, user=self.user)
        response = self.client.post(self.login_url, data, format="json")
        request.auth = response.data["id_token"]

        permission = IsAdminOrOwner()
        self.assertTrue(permission.has_permission(request, self.view))

    def test_has_permission_authenticated_user_without_token(self):
        request = self.factory.get("/api/accounts/")
        force_authenticate(request, user=self.user)
        permission = IsAdminOrOwner()

        self.assertFalse(permission.has_permission(request, self.view))

    def test_has_permission_unauthenticated_user(self):
        request = self.factory.get("/api/accounts/")
        request.auth = None
        request.user = AnonymousUser()

        permission = IsAdminOrOwner()
        self.assertFalse(permission.has_permission(request, self.view))

    def test_has_object_permission_admin_user(self):
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.save()
        data = {
            "username": self.user_data["username"],
            "password": self.user_data["password"],
        }
        request = self.factory.get("/api/accounts/")
        force_authenticate(request, user=self.user)
        response = self.client.post(self.login_url, data, format="json")
        request.META["HTTP_AUTHORIZATION"] = response.data["id_token"]

        permission = IsAdminOrOwner()
        self.assertTrue(permission.has_object_permission(request, self.view, self.test_object))

    def test_has_object_permission_non_admin_user(self):
        self.user.is_superuser = False
        self.user.is_staff = False
        self.user.save()
        data = {
            "username": self.user_data["username"],
            "password": self.user_data["password"],
        }
        with patch("backend.core.authentications.custom_JWT_token_authentication.CustomTokenAuthentication.jwt_decode_token") as mock_jwt_decode_token:
            mock_jwt_decode_token.return_value = {"auth": ["ROLE_USER"], "sub": "other_user"}

            # Authenticate the request as a non-admin user
            request = self.factory.get("/api/accounts/")
            request.user = self.user
            response = self.client.post(self.login_url, data, format="json")
            request.auth = response.data["id_token"]

            permission = IsAdminOrOwner()
            self.assertFalse(permission.has_object_permission(request, self.view, self.test_object))

    def test_has_object_permission_invalid_token(self):
        request = self.factory.get("/api/accounts/")

        payload = {
            "sub": self.user.username,
            "auth": "ROLE_USER",
            "exp": datetime.utcnow() - settings.JWT_AUTH["JWT_EXPIRATION_DELTA"],
        }
        token = jwt.encode(payload, settings.SECRET_KEY, algorithm="HS512", headers={"alg": "HS512"})
        force_authenticate(request, user=self.user, token=token)
        request.META["HTTP_AUTHORIZATION"] = str(token)[2:-1]
        with self.assertRaises(Exception) as err:
            permission = IsAdminOrOwner()
            permission.has_object_permission(request, self.view, self.test_object)

        self.assertEqual(str(err.exception), "Signature has expired")




