from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone
from rest_framework import exceptions
from rest_framework.test import APIRequestFactory
from rest_framework_jwt.settings import api_settings

from backend import settings
from backend.Accounts.helpers import find_roles_of_user
from backend.core.authentications.custom_JWT_token_authentication import CustomTokenAuthentication

UserModel = get_user_model()

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class CustomTokenAuthenticationTest(TestCase):
    """
        return: Test Class for Custom JWT Token Authentication.
    """

    def setUp(self):
        self.user_data = {
            "username": "testuser",
            "password": "qwertY@12345",
            "is_active": True,
        }
        self.user = UserModel.objects.create_user(**self.user_data)
        self.factory = APIRequestFactory()
        self.auth = CustomTokenAuthentication()

    def create_token(self, user, exp=None):
        custom_payload = {
            "sub": user.get_username(),
            "auth": ",".join(find_roles_of_user(user)[::-1]),
            "exp": timezone.now() + settings.JWT_AUTH["JWT_EXPIRATION_DELTA"],
            "user_id": self.user.id
        }
        if exp:
            custom_payload['exp'] = exp

        token = jwt_encode_handler(custom_payload)
        return token

    def test_successful_authentication(self):
        token = self.create_token(self.user)
        request = self.factory.get('/api/account/')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {token}'

        auth_user, auth_token = self.auth.authenticate(request)

        self.assertEqual(auth_user, self.user)
        self.assertEqual(auth_token, token)

    def test_invalid_token(self):
        request = self.factory.get('/api/account/')
        request.META['HTTP_AUTHORIZATION'] = 'Bearer invalid_token'

        with self.assertRaises(exceptions.AuthenticationFailed):
            self.auth.authenticate(request)

    def test_expired_token(self):
        expired_token = self.create_token(self.user, exp=datetime.utcnow() - timedelta(seconds=1))

        request = self.factory.get('/api/account/')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {expired_token}'

        with self.assertRaises(exceptions.AuthenticationFailed) as err:
            self.auth.authenticate(request)

        self.assertEqual(str(err.exception), "Token has expired.")

    def test_blacklisted_token(self):
        from rest_framework_jwt.blacklist.models import BlacklistedToken

        user = UserModel.objects.create_user(username='testuser2', password='password', email="email2@email.com")
        blacklisted_token = self.create_token(user)

        expires_at = datetime.utcnow() + settings.JWT_AUTH["JWT_EXPIRATION_DELTA"]
        BlacklistedToken.objects.create(token=blacklisted_token, expires_at=expires_at, user=user)

        request = self.factory.get('/api/account/')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {blacklisted_token}'

        with self.assertRaises(exceptions.PermissionDenied):
            self.auth.authenticate(request)

    def test_no_token(self):
        request = self.factory.get('/api/account/')
        request.META['HTTP_AUTHORIZATION'] = None
        result = self.auth.authenticate(request)

        self.assertIsNone(result)
        self.assertEqual(result, None)

    def test_invalid_signature(self):
        token = self.create_token(self.user)

        token = token[:-2] + 'XX'

        request = self.factory.get('/api/account')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {token}'

        with self.assertRaises(exceptions.AuthenticationFailed) as err:
            self.auth.authenticate(request)

        self.assertEqual(str(err.exception), 'Error decoding token.')

    def test_invalid_payload(self):
        token = self.create_token(self.user)

        idx = len(token) // 2
        token = token[:idx] + "X" + token[idx:]

        request = self.factory.get('/api/account')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {token}'

        with self.assertRaises(exceptions.AuthenticationFailed) as err:
            self.auth.authenticate(request)

        self.assertEqual(str(err.exception), 'Error decoding token.')

    def test_no_user_found(self):
        token = self.create_token(UserModel(username='nonexistent_user'))

        request = self.factory.get('/api/account')
        request.META['HTTP_AUTHORIZATION'] = f'Bearer {token}'

        with self.assertRaises(Exception) as err:
            self.auth.authenticate(request)

        self.assertEqual(str(err.exception), "Invalid token.")

    def test_no_blacklist_app_installed(self):
        with self.modify_settings(INSTALLED_APPS={'remove': 'rest_framework_jwt.blacklist'}):
            request = self.factory.get('/api/account/')
            token = self.create_token(self.user)
            request.META['HTTP_AUTHORIZATION'] = f'Bearer {token}'

            auth_user, auth_token = self.auth.authenticate(request)

            self.assertEqual(auth_user, self.user)
            self.assertEqual(auth_token, token)

# coverage report > test_report.txt --omit=tests/*
