import logging
import unittest

from backend.core.custom_filter.logging_filter import ExcludeUnnecessaryInformationFilter


class TestExcludeUnnecessaryInformationFilter(unittest.TestCase):
    """
        return: Test Class for testing Custom Filter for Logging functionality.
    """
    def setUp(self):
        self.filter = ExcludeUnnecessaryInformationFilter()

    def test_filter_file_message(self):
        record = logging.LogRecord(
            "test_logger",
            logging.WARNING,
            "pathname",
            1,
            "This is a file-related message.",
            None,
            None,
            "test_filter_file_message"
        )
        result = self.filter.filter(record)
        self.assertFalse(result)

    def test_filter_watching_message(self):
        record = logging.LogRecord(
            "test_logger",
            logging.INFO,
            "pathname",
            1,
            "Watching for updates...",
            None,
            None,
            "test_filter_watching_message"
        )
        result = self.filter.filter(record)
        self.assertFalse(result)

    def test_allow_other_messages(self):
        record = logging.LogRecord(
            "test_logger",
            logging.ERROR,
            "pathname",
            1,
            "An error occurred.",
            None,
            None,
            "test_allow_other_messages"
        )
        result = self.filter.filter(record)
        self.assertTrue(result)

